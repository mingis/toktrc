#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
toktrc.py
    Python interfeisas su LuaTeX toktrc.lua tokenų treiseriu.
    Ištraukia nurodytų TeX failo pozicijų būsenas kompiliavimo metu.
    Į texroot medį turi būti įdiegtas LuaTeX kompiliatorius su toktrc
    papildymais (žr. install_toktrc.bat ar install_toktrc.sh).
    Suformuoja reikalingus toktrc konfigūracijos parametrus –
    perrašo darbiniam aplanke esantį failą toktrc.cfg.
    Kompiliuoja nurodytą failą foo.tex LuaTeX kompiliatoriumi su toktrc
    papildymais.
    Iš gauto trace listingo foo.tex.toktrc.log išsitraukia ieškomas būsenas ir
    grąžina iškvietusiai programai.

Syntax:

Changelog:
    2013-10-07  mp  initial creation
    2013-10-08  mp  font decoding
    2013-10-09  mp  toktrc.lua
    2013-10-11  mp  AllTokens = Calls
    2013-10-29  mp  get_ms_stat() rezultato laukas 'Environment'

TODO:
    - get_ms_stat() binarinę įrašo paiešką, dabar nuosekli
        suradus artimiausią, dar važiuot atgal iki pirmo tokio paties,
        nes dabar jau tik pats pirmas atitinka pagrindinio failo tokeną,
        kiti gali būt gilesni macro iškvietimai
    - skanuoti foo.tex.used/undefined/unused.log'us
    - analizuoti foo.tex.log ir foo.log
"""

import os, sys
import subprocess
import shutil
import pprint
import configparser
from collections import OrderedDict

##############################
#    Konfiguracinio failo
#         objektas
##############################

from conf import Configurations
conf = Configurations()

# reliatyvus skripto kelias
module_path = os.path.dirname(os.path.abspath(__file__))

def ensure_dir(f):
    '''Uztikrina direktorijos buvima.'''
    # d = os.path.dirname(f)
    d = f
    if not os.path.exists(d):
        os.makedirs(d)
        print("\nSukurta direktorija: {}\n".format(d))

    return d


class TokenTracerError(Exception):
    pass

class TokTrc():

    cwd_name = 'vtexTMP'
    cfg_file_name = 'toktrc.cfg'

    def __init__(self, distribution, file_path, all_calls=False, CWD=None):
        """
        Constructor0

        distribution – int, TeX failo kompiliavimo distribucija VTEX texroot
        medyje – 2010, 2012 ir pan.

        file_path – str, analizuojamo TeX failo vardas, pvz., "foo.tex"

        all_calls – bool, ar naudoti visų makrokomandų iškvietimų analizę –
            reikialingas panaudotų makrokomandų vardų failo formavimui
            (foo.tex.used.log), bet mažina veikimo greitį.

        Konstruktorius nurodytą failą sukompiliuoja ir nuskanuoja sugeneruotą
            *.toktrc.log failą į stat_recs[]. Informacija apie norimas nurodyto
            TeX failo pozicijas po to gaunama galimai pakartotiniais metodo
            get_ms_stat() iškvietimais.

        Pakartotinis get_ms_stat() iškvietimas TeX failo neperkompiliuoja.
        Jeigu reikia tą patį failą perkompiliuoti, kuriamas naujas TokTrc
        objektas tuo pačiu .tex failo vardu.

        stat_recs[] įrašai – hash lentelės (dict) su laukų pavadinimais iš
        stat_rec_keys[]
        """

        super().__init__()

        self.distr = distribution
        self.file_path = os.path.abspath(file_path)

        if not os.path.exists(self.file_path):
            raise Exception("Nerastas failas {}!".format(self.file_path))

        self.fname = os.path.split(self.file_path)[1]

        ################################
        # Darbines direktorijos kelias
        ################################

        if not CWD:
            self.CWD = ensure_dir(os.path.join(os.path.dirname(file_path),
                                  self.cwd_name))
        else:
            self.CWD = ensure_dir(CWD)

        self.cfg_file = os.path.join(self.CWD, 'toktrc.cfg')
        shutil.copy(os.path.join(module_path, 'toktrc.lua'),
                    self.CWD)
        shutil.copy(os.path.join(module_path, 'tokinit.lua'),
                    self.CWD)
        shutil.copy(os.path.join(module_path, 'configparser.lua'),
                    self.CWD)

        self.log_file_suff = "toktrc.log"
        self.log_fname = os.path.join(self.CWD,
                                      self.fname + "." + self.log_file_suff)

        ##############################
        #   Environmento klonas
        ##############################

        my_env = os.environ.copy()
        my_env['TEXMFCNF'] = os.path.normpath(conf.get('texmfcnf'))

        ##############################
        #    Komandines eilutes
        #       suformavimas
        ##############################

        # kompiliatoriaus kelias nurodytas config.ini faile
        if sys.platform == 'win32':
            compiler = os.path.normpath(
                os.path.join(os.path.join(module_path, 'common'), sys.platform))
        else:
            compiler = os.path.normpath(
                os.path.join(module_path, 'common/linux'))

        my_env['PATH'] = compiler + ';' + my_env['PATH']

        # kelias iki lua skripto
        lua_script = os.path.join(module_path, "toktrc.lua")


        if sys.platform == 'win32':
            compiler = os.path.join(compiler, 'luatex')
            options = ["--fmt=lualatex",
                       "--progname=latex",
                       "--recorder",
                       "--interaction=nonstopmode",
                       "--lua=" + lua_script]
        else:
            compiler = os.path.join(compiler, 'lualatex')
            options = ["--lua=toktrc.lua"]


        self.file_path = self.file_path.replace('\\','/')

        cmd_line = [compiler] + options + [self.file_path]

        # *.toktrc.log failo įrašo ir get_ms_stat() grąžinamos lentelės
        # laukų pavadinimai
        self.stat_rec_keys = ['tokcnt', 'filename', 'lnum', 'lpos',
            'tcmd', 'cmdname', 'tchcode', 'text', 'tid', 'grplev',
            'mathmode', 'ixlev', 'font', 'page', 'environment']

        # Lua masyvo font.fonts[] įrašo laukų pavadinimai
        self.font_rec_keys = [
            'name', 'area', 'used', 'characters', 'checksum', 'designsize',
            'direction', 'encodingbytes', 'encodingname', 'fonts', 'psname',
            'fullname', 'header', 'hyphenchar', 'parameters', 'size',
            'skewchar', 'type', 'format', 'embedding', 'filename', 'tounicode',
            'stretch', 'shrink', 'step', 'auto_expand', 'expansion_factor',
            'attributes', 'cache', 'nomath', 'slant', 'extent']

        """
        nuskanuotos *.toktrc.log failo reikšmės,
        suformuoja scan_log_file()
        """
        self.stat_recs = []

        # ---------
        self.make_tok_trc_cfg(all_calls) # gaminamas toktrc.cfg

        live = subprocess.Popen(cmd_line,
                                env=my_env,
                                # stdout=subprocess.PIPE,
                                cwd=self.CWD)

        turinys = live.communicate()[0]
        print(turinys)

        # self.scan_log_file() # iš *.tokrc.log formuojamas stat_recs[]


    def make_tok_trc_cfg(self, all_calls):
        """
        Sukuria failą toktrc.cfg su pilnu trasavimo elementų sąrašu

        all_calls – bool, žr. konstruktorių
        """

        config = configparser.ConfigParser(dict_type=OrderedDict)
        # tam, kad butu case sensitive
        config.optionxform = str

        MainFileName  = self.fname
        TraceLevel    = 'tracer'
        TraceFileName = 'yes'
        AllTokens     = 'calls' if all_calls else 'no'
        TraceTokCnt   = 'all'
        TraceLpos     = "yes"
        TraceTcmd     = 'yes'
        TraceCmdName  = 'yes'
        TraceTchCode  = 'yes'
        TraceText     = 'yes'
        TraceTid      = 'yes'
        TraceGrpLev   = 'yes'
        TraceMathMode = 'yes'
        TraceIxLev    = 'yes'
        TraceFont     = 'full'
        TracePage     = 'yes'
        TraceEnv      = 'yes'

        config['DEFAULT'] = OrderedDict((
            ('mainfilename', MainFileName),
            ('tracelevel', TraceLevel),
            ('tracefilename', TraceFileName),
            ('alltokens', AllTokens),
            ('tracetokcnt', TraceTokCnt),
            ('tracelpos', TraceLpos),
            ('tracetcmd', TraceTcmd),
            ('tracecmdname', TraceCmdName),
            ('tracetchcode', TraceTchCode),
            ('tracetext', TraceText),
            ('tracetid', TraceTid),
            ('tracegrplev', TraceGrpLev),
            ('tracemathmode', TraceMathMode),
            ('traceixlev', TraceIxLev),
            ('tracefont', TraceFont),
            ('tracepage', TracePage),
            ('traceenv', TraceEnv)))

        self.cfg_file_path = os.path.join(self.CWD, self.cfg_file_name)
        with open(self.cfg_file_path, 'w') as configfile:
            config.write(configfile)

    ##############################
    #   Lua grazintu log failu
    #    parsinimo funkcijos
    ##############################

    def parse_font(self, String):
        returnas = {}
        font_props = String.split('/')
        for nr,value in enumerate(font_props):
            font_key = self.font_rec_keys[nr]
            returnas[font_key] = value

        return returnas

    def parse_index_level(self, String):
        returnas = {}
        ix_levs = String.split('/')
        for nr, i in enumerate(ix_levs):
            returnas[nr] = i

        return returnas

    def scan_log_file(self):
        """ Nuskanuoja *.toktrc.log failą į stat_recs[] """

        with open(self.log_fname, 'rt', encoding='utf8') as log_file:
            log_lines = log_file.read().splitlines()
            log_lines = [i.rstrip('\t \n').split('\t') for i in log_lines]

        # Patikrinama ar sutampa lauku pavadinimai
        if not self.stat_rec_keys == log_lines.pop(0):
            raise TokenTracerError('Blogas failo formatas!')

        self.stat_recs = []

        for lnr, line in enumerate(log_lines):
            self.stat_recs.append({})
            for nr, field in enumerate(line):
                field_name = self.stat_rec_keys[nr]

                if field_name == 'font':
                    value = self.parse_font(field)
                elif field_name == 'ixlev':
                    value = self.parse_index_level(field)
                else:
                    value = field

                self.stat_recs[lnr][field_name] = value


if __name__ == '__main__':
    tracer = TokTrc('2010', 'test.tex')
