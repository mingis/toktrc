Help on class TokTrc in module toktrc:

class TokTrc(kppylib.common.kpcapp.KpCapp)
 |  # ---------------------------------
 |  
 |  Method resolution order:
 |      TokTrc
 |      kppylib.common.kpcapp.KpCapp
 |      __builtin__.object
 |  
 |  Methods defined here:
 |  
 |  __init__(self, distribution, file_name, all_calls=False)
 |      Constructor
 |      
 |      @param distribution – int, TeX failo kompiliavimo distribucija VTEX
 |          texroot medyje – 2010, 2012 ir pan.
 |      
 |      @param file_name – str, analizuojamo TeX failo vardas, pvz., "foo.tex"
 |      
 |      @param all_calls – bool, ar naudoti visų makrokomandų iškvietimų
 |          analizę – reikialingas panaudotų makrokomandų vardų failo
 |          formavimui (foo.tex.used.log), bet mažina veikimo greitį.
 |      
 |      Konstruktorius nurodytą failą sukompiliuoja ir nuskanuoja sugeneruotą
 |          *.toktrc.log failą į self.stat_recs[]. Informacija apie norimas
 |          nurodyto TeX failo pozicijas po to gaunama galimai pakartotiniais
 |          metodo get_ms_stat() iškvietimais.
 |      
 |      Pakartotinis get_ms_stat() iškvietimas TeX failo neperkompiliuoja.
 |      Jeigu reikia tą patį failą perkompiliuoti, kuriamas naujas TokTrc
 |      objektas tuo pačiu .tex failo vardu.
 |      
 |      self.stat_recs[] įrašai – hash lentelės (dict) su laukų pavadinimais
 |          iš self.stat_rec_keys[]
 |  
 |  get_ms_stat(self, lnum, lpos)
 |      Ištraukia nurodytos TeX failo self.fname pozicijos būseną kompiliavimo
 |          metu.
 |      
 |      @param lnum, lpos – int, analizuojamo TeX failo pozicija, apie kurią
 |          norime sužinoti informaciją. Failo eilutės numeruojamos nuo 1,
 |          pozicijos eilutėje – nuo 0. Pozicijos eilutėje skaičiuojamos
 |          baitais, t.y., UTF-8 koduotam failui kol kas bus problemų.
 |      
 |      @return ret_val     grąžina hash lentelę (dict, artimiausią
 |          self.stat_recs[] įrašą), susidedančią iš laukų, kurių reikšmių
 |          prasmė atitinka *.toktrc.log failo laukus, detaliau aprašytus
 |          failo skaityk_toktrc.txt skyrelyje „Pranešimų failo laukų
 |          reikšmės“. Laukų pavadinimai yra masyve stat_rec_keys[]
 |          Laukų reikšmių tipai ir skirtumai nuo *.toktrc.log įrašo
 |          struktūros:
 |      
 |              'tokcnt'        – int
 |              'filename'      – str
 |              'lnum'          – int
 |              'lpos'          – int
 |              'tcmd'          – str
 |              'cmdname'       – str
 |              'tchcode'       – int
 |              'text'          – str
 |              'tid'           – int
 |              'grplev'        – int
 |              'mathmode'      – int
 |              'ixlev'         – list *
 |              'font'          – dict **
 |              'page'          – int
 |              'environment'   – str
 |              'diff'          – int ***
 |              'fpos'          – int ****
 |      
 |        * Matematinio indekso gyliui apibūdinti grąžinama ne teksto eilutė
 |              (kaip *.toktrc.log atveju), o int elementų masyvas. Jeigu
 |              nurodyta pozicija nėra indekso viduje, bus grąžintas vienas
 |              nulinis elementas (skaičius 0). Jeigu indeksai įdėtiniai, bus
 |              grąžinamas vienas elementas, nurodantis indekso gylį.
 |              Apatiniai indeksai pažymimi neigiama gylio reikšme. Daugiau
 |              negu vienas elementas grąžinamas sudėtinių indeksų atveju,
 |              pvz., A_{B^{C_{D_E}}} atveju raidės "E" tokeno lygmuo bus
 |              [-1, 1, -2].
 |      
 |       ** Einamajam fontui apibūdinti grąžinama ne teksto eilutė, o hash
 |              lentelė – LuaTeX fontų masyvo font.fonts įrašo atitikmuo su
 |              laukais "name", "size", "designsize" ir kt., žr. failo
 |              http://www.luatex.org/svn/trunk/manual/luatexref-t.pdf
 |              7-ą skyrių.
 |      
 |      *** Grąžinamas papildomas laukas "diff", jo reikšmė – atstumas tarp
 |              užsakytos analizavimo pozicijos lnum, lpos ir realiai
 |              faile rastos artimiausios tokeno pradžios pozicijos
 |              get_ms_stat()["lnum"], get_ms_stat()["lpos"]. Laukas skirtas
 |              atpažinti situacijas, kai nurodytos pozicijos faile nebuvo.
 |              Jei faile buvo rasta tiksliai sutampanti pozicija, grąžinama
 |              nulinė lauko reikšmė. Jei ne – grąžinamas absoliutus skirtumas
 |              tarp pozicijų eilutėje, o jei ir eilutės nesutapo – pridedamas
 |              dar ir absoliutus eilučių numerių skirtumas, padaugintas iš
 |              1000.
 |      
 |      **** Grąžinamas papildomas laukas "fpos", jo reikšmė – rastos
 |              pozicijos absoliutus atstumas nuo failo pradžios baitais.
 |              Pirmo failo baito pozicija 0, eilučių galai traktuojami kaip
 |              užimantys visi po 1 baitą.
 |  
 |  get_ms_stat_abs(self, fpos)
 |      Ištraukia nurodytos TeX failo self.fname pozicijos būseną kompiliavimo
 |          metu.
 |      
 |      @param fpos – int,  absoliuti baito pozicija nuo analizuojamo TeX
 |          failo pradžios, apie kurią norime sužinoti informaciją. Pirmo
 |          failo baito pozicija 0, eilučių galai traktuojami kaip užimantys
 |          visi po 1 baitą.
 |      
 |      @return ret_val     rezultato hash lentelė (dict, žr. get_ms_stat()).
 |          Grąžinamo lauko ret_val["diff"] reikšmė šiuo atveju – tiesiog
 |          absoliutus skirtumas tarp ieškomos nurodytos pozicijos fpos ir
 |          rastos artimiausios tokeno pozicijos faile.
 |  
 |  make_tok_trc_cfg(self, all_calls)
 |      Sukuria failą toktrc.cfg su pilnu trasavimo elementų sąrašu 
 |      
 |      @param all_calls – bool, žr. konstruktorių
 |  
 |  recompile(self, all_calls=False)
 |      Perkompiliuoja TeX failą self.fname
 |      
 |      @param all_calls – žr. konstruktorių
 |      
 |      Suformuoja self.stat_recs[], žr. konstruktorių
 |  
 |  scan_log_file(self)
 |      Nuskanuoja *.toktrc.log failą į self.stat_recs[]
 |  
 |  scan_tex_file(self)
 |      Skanuoja TeX išeities failą, susiskaičiuoja eilučių pozicijas į
 |          self.tex_lpositions[]
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors inherited from kppylib.common.kpcapp.KpCapp:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
 |  
 |  ----------------------------------------------------------------------
 |  Data and other attributes inherited from kppylib.common.kpcapp.KpCapp:
 |  
 |  os_pars = {'ux': {'Call': '', 'Copy': 'cp', 'Del': 'rm', 'ExeExt': '',...

