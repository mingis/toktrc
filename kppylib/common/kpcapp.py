#!D:\bin\python26\python.exe
# coding=UTF-8

# --------------------------------
"""
kp common application
"""

import os, sys
import configparser
from kppylib.env.common.kperr import KpError, KpException, \
                                            KpKeywordNotFoundError

# --------------------------------------
#   lr  konfigūracijos failo parseris
class Config():
    '''Konfiguraciju objektas.'''

    in_file_name = 'config.ini'

    def __init__(self):
        self.dir = os.path.abspath(os.path.dirname(__file__))
        self.static_file = os.path.join(self.dir, self.in_file_name)

        local_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
        local_ini_file = os.path.join(local_dir, self.in_file_name)
        if (os.path.exists(local_ini_file)):
            self.static_file = local_ini_file

        local_dir = os.path.abspath(os.path.dirname("."))
        local_ini_file = os.path.join(local_dir, self.in_file_name)
        if (os.path.exists(local_ini_file)):
            self.static_file = local_ini_file

    def get(self, name):
        '''Istraukia kintamojo vardu name reiksme is config.ini
        failo, '''

        config = configparser.ConfigParser()
        config.read(self.static_file)

        if sys.platform == 'win32':
            static_settings = dict(config['WIN'])
        else:
            static_settings = dict(config['LINUX'])

        KpError.asserte(name in static_settings.keys(),
            KpKeywordNotFoundError(
                'Nerastas konfiguracinis kintamasis: {}'.format(name)))

        return static_settings[name]


# ----------------------------------
class KpCapp(object):

    """ parameters depending on os """
    os_pars = \
    {
        'win':
        {
            # folders delimiter in a path
            'OsPathDelim': "\\",

            'Copy': "copy",
            'Del': "del /Q",
            'Call': "call ",
            'ExeExt': ".exe"
        },
        'ux':
        {
            'OsPathDelim': "/",
            'Copy': "cp",
            'Del': "rm",
            'Call': "",
            'ExeExt': ""
        }
    }

    def __init__(self):

        self.os_type = 'ux' 
        try:
            if ((os.environ['windir'] != None) and (os.environ['windir'] != "")):
                self.os_type = 'win' 
        except:
            pass

        # ----------------------
        # config.ini parsinimas
        self.conf = Config()
