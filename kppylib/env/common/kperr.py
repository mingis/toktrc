#!D:\bin\python26\python.exe
# coding=UTF-8


# --------------------------------------
from __future__ import print_function
import sys, traceback

#
# Usage:
#
#   try:
#       ...
#   except Exception, exc:
#       KpError.catch(exc)
#   except SystemExit, exc:
#       if (str(exc) != "0"):
#           KpError.error(KpError.KP_E_SYSTEM_ERROR, "Exit code: " + str(exc), True)
#   except:
#       KpError.error(KpError.KP_E_SYSTEM_ERROR, "Unhandled exception", True)
#


# --------------------------------------
class KpException(Exception):

    err_code = None # int: KpError.KP_E_...
    err_msg = None # string

    def __init__(self, error_code, error_message):
        self.err_code = error_code
        self.err_msg = error_message


class KpInvalidArgError(KpException):
    def __init__(self, error_message = ""):
        super(KpInvalidArgError, self).__init__(KpError.KP_E_INVALIDARG, error_message)

class KpFileFormatError(KpException):
    def __init__(self, error_message = ""):
        super(KpFileFormatError, self).__init__(KpError.KP_E_FILE_FORMAT, error_message)

class KpNoFileError(KpException):
    def __init__(self, error_message = ""):
        super(KpNoFileError, self).__init__(KpError.KP_E_NO_FILE, error_message)

class KpSystemError(KpException):
    def __init__(self, error_message = ""):
        super(KpSystemError, self).__init__(KpError.KP_E_SYSTEM_ERROR, error_message)
    
class KpFileNotFoundError(KpException):
    def __init__(self, error_message = ""):
        super(KpFileNotFoundError, self).__init__(KpError.KP_E_FILE_NOT_FOUND, error_message)

class KpCancelError(KpException):
    def __init__(self, error_message = ""):
        super(KpCancelError, self).__init__(KpError.KP_E_CANCEL, error_message)

class KpEndOfFileError(KpException):
    def __init__(self, error_message = ""):
        super(KpEndOfFileError, self).__init__(KpError.KP_E_END_OF_FILE, error_message)

class KpKeywordNotFoundError(KpException):
    def __init__(self, error_message = ""):
        super(KpKeywordNotFoundError, self).__init__(KpError.KP_E_KWD_NOT_FOUND, error_message)

# --------------------------------------
class KpError:

    KP_E_INVALIDARG     = 0
    KP_E_FILE_FORMAT    = 1
    KP_E_NO_FILE        = 2
    KP_E_SYSTEM_ERROR   = 3
    KP_E_FILE_NOT_FOUND = 4
    KP_E_CANCEL         = 5
    KP_E_END_OF_FILE    = 6
    KP_E_KWD_NOT_FOUND  = 7

    messages = [
        "Blogi argumentai",        # 0 KP_E_INVALIDARG
        "Blogas failo formatas",   # 1 KP_E_FILE_FORMAT
        "Failas neatvertas",       # 2 KP_E_NO_FILE
        "Sisteminė klaida",        # 3 KP_E_SYSTEM_ERROR
        "Failas nerastas",         # 4 KP_E_FILE_NOT_FOUND
        "Operacija nutraukta",     # 5 KP_E_CANCEL
        "Failo pabaiga",           # 6 KP_E_END_OF_FILE
        "Raktažodis nerastas",     # 7 KP_E_KWD_NOT_FOUND
    ]

    @staticmethod
    def warning(error_code, error_message = ""): # former Warning()
        print("Warning: " + KpError.messages[error_code] + ": " + error_message, file=sys.stderr)

    @staticmethod
    def error(error_code, error_message = "", was_exception = False): # former Error()
        out_msg = KpError.append_msg("! Error", KpError.messages[error_code], ": ")
        out_msg = KpError.append_msg(out_msg, error_message, ": ")
        print(out_msg, file=sys.stderr)
        if (was_exception): 
            # traceback.print_exc()
            tback_lines = traceback.format_exc().split("\n")
            for ii in range(1, len(tback_lines) - 2):
                if (ii % 2 == 1):
                    if (tback_lines[ii].find("kperr") < 0):
                        print(tback_lines[ii], file=sys.stderr)
        else:
            # traceback.print_stack(myProgramFile)
            # traceback.print_stack()
            tback_recs = traceback.format_stack()
            for ii in range(len(tback_recs)): # - 1):
                out_str = tback_recs[ii].split("\n")[0]
                if (out_str.find("kperr") < 0):
                    print(out_str, file=sys.stderr)

    @staticmethod
    def log_msg(error_message): # former LogMsg()
        print(error_message)

    @staticmethod
    def append_msg(out_msg, error_message, delim = "; "): # former AppendMsg()
        """ Appends error_message to out_msg and returns new concatenated string """
        new_msg = out_msg 
        if ((new_msg == None) or (new_msg == "")): new_msg = error_message
        else:
            if (error_message != None) and (error_message != ""):
                new_msg += delim + error_message
        return new_msg         

    @staticmethod
    def catch(exc): # former Catch
        err_cd = KpError.KP_E_SYSTEM_ERROR
        out_msg = ""
        if ((type(exc) is KpException) or (issubclass(type(exc), KpException))):
            err_cd = exc.err_code 
            out_msg = KpError.append_msg(out_msg, exc.err_msg)
        else:
            if (type(exc) is KeyError): 
                err_cd = KpError.KP_E_INVALIDARG
                out_msg = KpError.append_msg(out_msg, "Key is not in the dictionary")
            elif (type(exc) is WindowsError): 
                if(exc.args[0] == 2):
                    err_cd = KpError.KP_E_FILE_NOT_FOUND
                else:
                    out_msg = KpError.append_msg(out_msg, "WindowsError " + str(exc.args[0]))
                out_msg = KpError.append_msg(out_msg, exc.args[1])
            elif (type(exc) is KeyboardInterrupt): 
                err_cd = KpError.KP_E_CANCEL

            out_msg = KpError.append_msg(out_msg, str(exc))
            # out_msg = KpError.append_msg(out_msg, str(type(exc)))
                
        # print(exc.args)
        # out_msg = KpError.append_msg(out_msg, str(exc.args))
        
        if (hasattr(exc, 'child_traceback')): 
            # print(exc.child_traceback) # subprocess.Popen child process stack # OSError?
            out_msg = KpError.append_msg(out_msg, str(exc.child_traceback))
        
        KpError.error(err_cd, out_msg, True)

        sys.exit()
        
    @staticmethod
    # def Assert(cond, error_code, error_message = ""): 
    #     if(not cond): raise KpException(error_code, error_message)
    def asserte(cond, exc): # former Assert()
        """ exc could be inherited from KpException """ 
        # assert cond, KpException() – meta vis tiek AssertionError, gal čia dėl Python 2.6 versijos? 
        if(not cond): raise exc
      
    @staticmethod
    # def AssertW(cond, error_code, error_message = ""): 
    #     if(not cond): KpError.warning(error_code, error_message)
    def assertw(cond, exc): # former AssertW()
        """ exc should be inherited from KpException """ 
        if(not cond): KpError.warning(exc.err_code, exc.err_msg)
