% SPRINGER
%   Global Layout Specification:  
%   Small condensed
%
% TeX programming: Vytas Statulevicius, VTeX, Lithuania, vytas@vtex.lt
% Requires Latex2e, ver.2000.06
%
% 2006.09.14 - started
% id="SmallLayout"    ver. 1.2.2  2006.05.29
% id="SmallExtLayout" ver. 1.1    2006.05.29
% 2006.11.06 - ivesta komanda \lcline
% 2006.11.07 - pataisyta lenteles mediumwidth skaiciavimas
% 2006.11.21 - ivesta komanda \springeronline
% 2006.11.22 - \maxdepth nustatytas 0pt
% 2006.12.11 - i id eilute irasytas layout tekstas
%            - paper@height nustatytas 290mm
% 2006.12.12 - atjungtas marvosymb paketo krovimas
% 2006.12.13 - komanda \url neformuoja link'o
% 2006.12.14 - ivesta komanda \tabnotesinline;
%            - sutvarkyta sidewaystable
%            - sutvarkyta klaida thm aplinkose kai numeravimas priklauso nuo section
%            - PROOF modeoje isvedamas id per RH mechanizma
%            - ijungtas citesort
%            - ideti raktai \ead[fax]{}, ead[phone]{}
% 2006.12.15 - i id eilute idetas journal@number
% 2006.12.22 - ivesta aplinka {biography}
%            - ivestas raktas presentaddressref
% 2006.12.28 - sutvarkyta history
% 2007.01.03 - ivestas raktas nocorref: \printauthor[nocorref={au1,au2}]{au1,au2}
% 2007.01.09 - pries accepted, pubonline dedamas " / "
% 2007.01.10 - timessi pakeistas i timesxi (+ status {}[]0-9)
% 2007.01.11 - ideta \postbox komanda
% 2007.01.12 - tabnotes numeruojamos a,b.. + sumazintas tarpas pries firsttabnote
%            - pakeistas \figurename
% 2007.01.16 - \ignorespaces prie \endtabular pridedamas su \AtBeginDocument (kad veiktu su array.sty)
%            - i {ack}, {acks} {artnote} idetas \par gale
%            - ivesta komanda \newremark
%            - ivesta komanda \revised{}
% 2007.01.17 - tikrinama ar \author komandoje yra nuoroda (addressref=) i \address (id=)
%            - tikriname ar biography aplinkos authorref yra egzistuojantis + jis isvedamas parasteje HPROOF modoje
% 2007.01.19 - prijugtas palaikymas "nobreaklinks" (nedarome linku uztempimo)
% 2007.01.22 - pataisyta klaida su \cite spalvinimu;
%            - \springeronline isjungia cite spalvinima
%            - nuimtas : DOI; pakeista MSC ;
% 2007.01.23 - ivesti keyword raktai JEL, PACS
%            - amsthm modoje pataisyta remark
%            - idetas tikrinimas ar author, address id yra unikalus
%            - ijungtas raktas nobreaklinks
% 2007.01.25 - is \noharm isimtas \let~\space
%            - sutvarkytas \contcaption{}
% 2007.01.26 - pakeistas figurecaption@fmt
%            - atstatytas MSC
% 2007.01.31 - nameyear atveju cite@sep pakeistas i ";"
% 2007.02.01 - sutvarkytas biography (gale \par)
% 2007.02.02 - idetas lastpage tikrinimas PROOF modoje
% 2007.02.05 - paragraph prie numerio nededa tasko
% 2007.02.06 - ideta online aplinka
% 2007.02.12 - ivesta komanda \printaug[]{au1,au2}{aff1}{e1,e2}
%            - ivesta komanda \bioauthorname
%            - ivesti raktai auprefix, addrprefix
%            - keyval paketas netikrina raktu apibreztumo
% 2007.02.13 - pataisyta \check@aug
%            - idetas \smart@par
% 2007.02.15 - pakeistas ESMHint@text
% 2007.02.20 - present address isvedamas pries autoriaus pavarde;
%            - pakeistas ESMHint@text
%            - pakeista frontmatter@cmd
% 2007.02.21 - pataisytas bugas presentaddressref
%            - \fminsert apibreztas kaip long
%            - pakeistas journal@info
% 2007.02.22 - jei nera ead id: \printaug{au1}{aff}{} corref atributai nuvalomi
%            - tikriname ar visi \ead buvo isvesti su \printead (\check@ead)
%            - ivesta komanda \doiurl{http://dx.doi.org/...} arba \doiurl{...}
%            - \printaug: jei nei vienas autoriu nera corresponding ir autoriu>1 po adreso email'as nespausdinamas
%            - \springeronline ijungia \hideuncited
%            - number literaturos atveju \cite gali buti tik: \cite[Petrov \byear{1999}]{r1}
%            - raktas noeadcheck (jei nera email'u!)
% 2007.02.23 - ivesti kontroles raktai standardrh, optionalrh
%            - idetos komados \cyr ir \textcyr
% 2007.02.26 - idetas raktas norhcheck
% 2007.02.28 - pataisyta sidewaystable
%            - ideta komanda \corrigendum
% 2007.03.03 - padidintas zenkliukas \letter
%            - \dochead ijungtas letterspacing (SOUL)
% 2007.03.04 - klase suskaidyta i spr-common-fm.sty, spr-common-flt.sty, spr-common-body.sty
%            - ivestas \authorminrmargin@width
%            - ivestas \runninghead@width            
% 2007.03.05 - padidintas zenkliukas \letter
%            - sumazintas interletter spacing (SOUL) 
% 2007.03.06 - idetas \g@addto@macro\CRC\springeronline
% 2007.03.07 - idetas raktas noartcheck
% 2007.03.09 - nuimtas prefiksas "~/ " is accepted@prefix etc.
% 2007.03.13 - idetas \TrimBox
% 2007.03.14 - link'u spalva nustatyta i [0 0 1]
%            - prijungtas paketas stfloats
% 2007.03.15 - prijungtas paketas spr-common-prelims
%            - pataisyta \cite[] number atveju
% 2007.03.26 - pataisytas endfmbox
% 2007.03.27 - idetas surisimas Published~online
% 2007.04.05 - ideti raktai: kwdrequired, jelrequired, mscrequired, pacsrequired
%            - nokwdcheck - tikrinimo isjungimas
% 2007.04.21 - springeronline modoje ir CRC ijungtas \nokwdcheck
% 2007.04.24 - pakeistas \title: galima \title[]{}
% 2007.04.26 - ideta opcija nooutlines (isjungia bookmarku generavima)
%            - idetos opcijos artlangcheck, noartlangcheck (ijungiama zurnalams kuriems privaloma
%              komanda \artlang{}
% 2007.05.04 - ivestas raktas noaugcheck (netikrina corref)
% 2007.05.23 - ivestas dochead@skip
% 2007.06.05 - ivesta opcija v1.1 (stiliaus parametrai atitinka spec. Small v.1.2, 2007.02.12)
%            - pataisytas \Bigg
% 2007.09.25 - pakeistas ESMHint@text
% 2007.09.26 - ivesta opcija UStrimsize (6 1/2 x 9 1/2)
% 2007.10.02 - pakeistas abstract@dkip (buvo 0pt)
% 2007.10.17 - amsmath opcijoje idetas \allowdisplaybreaks
% 2007.11.13 - idetas "kablys" onlinelogo@text
% 2007.11.19 - 1pt sumazintas medium@width
% 2007.12.28 - pakeistas title@fmt
% 2008.01.04 - ivestas raktas noartlangcheck
% 2008.01.16 - idetas parametras title@rule
% 2008.01.29 - ivestas raktas nospinoffcheck
% 2008.02.01 - nustatyti issue@label, volume@label (naudojama spinoff)
% 2008.02.06 - \special{papersize=...} perkeltas is AtBeginDvi i AtBeginDocument
% 2008.03.28 - ivestas kablys pretitle@text
% 2008.03.28 - ijungti isoriniai link'ai
% 2008.04.01 - isjungti isoriniai linkai
% 2008.04.11 - prijungtas paketas vtexurl
% 2008.04.14 - nuimtas grid HPROOF modoje
% 2008.04.16 - ijungti isoriniai link'ai
% 2008.04.26 - nuo straipsnio doi neformuojamas link'as
% 2008.05.09 - ijungtas raktas autobibl
% 2008.05.27 - pataisyta klaida su raktu nobookmarks
% 2008.08.07 - ijungtas tikrinimas \check@addr
% 2008.08.27 - gutter margin padidintas 1pt
% 2008.11.10 - pakeista id eilute
% 2008.11.14 - \springeronline versijoje isvedame dummy logo
% 2008.11.17 - istaisyta klaida del \springeronline
% 2009.04.07 - ivesta opcija 168x240trim
% 2009.09.17 - ivesta opcija bleedbox (spalvotu puslapiu spausdinimui)
% 2009.11.21 - nustatyti alttitle ir altsubtitle parametrai
%            - ivesta opcija altlayout
% 2009.12.04 - pakeistas \alttitle
% 2009.12.17 - prijungtas paketas spr-common-pdfsettings
% 2009.12.22 - tikriname ar versija 1.1 + komanda \noversioncheck
% 2010.02.02 - ivestas \frontmatter@skip
%              PROBLEMA: sumuojasi tarpai kai nieko nera po \end{fmbox}
%              Reikia tikrinti ir jei nera abstract, keyword, abbr \frontmatter@skip pril. 0 
% 2010.02.03 - bendros options perkeltos i spr-common-options stiliu
%            - pagrazintas kodas
% 2010.09.01 - isjungtas \marginparsep
% 2010.09.23 - ivestos opcijos MSC2010, MSCnoyear
% 2010.10.08 - letterspacing padaytas specialaus srifto pagalba
% 2011.01.18 - pakeisti dochead plociai
% 2011.01.20 - prijungtas vtexfltcheck
%            - lastpage pakeistas vslastpage
% 2011.05.18 - pataisytas copyright@text
% 2011.05.19 - pataisytas runninghead@text
% 2011.08.16 - ivesta opcija v1.2
% 2011.08.17 - ivestas komandos \acid{} palaikymas
% 2011.10.11 - kraunamas paketas spr-common-xml
% 2011.12.23 - ivesta opcija v1.3
% 2012.01.10 - ideta opcija checkcaption
% 2012.01.13 - pakeista t1ptmspac.fd 
% 2012.01.18 - pakeisti altsubtitle parametrai altlayout atveju
% 2012.01.24 - corref zenkliukas idetas i \hbox
% 2012.02.28 - ideta \renewcommand{\big}{\bBigg@{1.1}}
% 2012.11.23 - Live 2010 dist. kitaip marvosym paketas
% 2012.11.28 - pataisyta latex006 formatui ()
% 2012.12.04 - tikriname ar TeX Live 2010 distr. ir tada kraunam marvosym paketa
% 2012.12.07 - idetas \set@rh@hook
% 2012.12.13 - ijungtas raktas autorh
% 2013.03.14 - bibliografija iskelta i spr-common-bibl
% 2013.04.04 - idetas tikrinimas ar autoriai uzima >3 eil. \check@aug@height (OTRS 17214)
% 2013.12.17 - idetas raktas nocorref (DG)

\def\fmt@name{spr-small-v1}
\def\fmt@version{2013/12/17}

\def\fmt@subversion{}

\NeedsTeXFormat{LaTeX2e}[2000/06/02]
\ProvidesClass{spr-small-v1}
              [\fmt@version, Springer Small condensed  and Small Extended layout]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% load package with common options and definitions

\RequirePackage{spr-common-options}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - UStrimsize


% US trim size option:
\let\set@trim@size@hook\relax
\let\set@margins@hook\relax

% US trim size 6 1/2 x 9 1/2
\def\set@UStrimsize{%
  \def\trim@width   {6.5in}
  \def\trim@height  {9.5in}
  \def\trim@bounding@box{0 0 468 684}
                   }

\def\set@USmargins{%
  \setlength\oddsidemargin{24mm}% gutter margin
  \setlength\@tempdima\trim@width
  \advance\@tempdima by-\textwidth
  \advance\@tempdima by-\oddsidemargin
  \setlength\evensidemargin\@tempdima
}

\DeclareOption{UStrimsize}{%
   \let\set@trim@size@hook\set@UStrimsize%
   \let\set@margins@hook\set@USmargins}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - 168x240trim
% Kluwer (old) trim size 168 x 240 mm

\def\set@KLUWtrimsize{%
  \def\trim@width   {168mm}
  \def\trim@height  {240mm}
  \def\trim@bounding@box{0 0 476.2205 680.3150}
                   }

\def\set@KLUWmargins{%
  \setlength\oddsidemargin{23mm}% gutter margin
  \setlength\@tempdima\trim@width
  \advance\@tempdima by-\textwidth
  \advance\@tempdima by-\oddsidemargin
  \setlength\evensidemargin\@tempdima
}

\DeclareOption{168x240trim}{%
   \let\set@trim@size@hook\set@KLUWtrimsize%
   \let\set@margins@hook\set@KLUWmargins}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - layout
% Layout options
\def\layout@version{v.1.1}
\def\layout@text{Small Condensed \layout@version}
\newif\if@extendedlayout \@extendedlayoutfalse

\DeclareOption{extended}{\def\layout@text{Small Extended \layout@version}\@extendedlayouttrue}

% Switch for layout
\newif\if@small@layout
\@small@layouttrue


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - class versiom
% version options
\newif\if@version@xi \@version@xifalse
\DeclareOption{v1.1}{\@version@xitrue\def\fmt@subversion{.1}\def\layout@version{v.1.2}}

\DeclareOption{v1.2}{%
  \@version@xitrue%
  \def\fmt@subversion{.2}%
  \def\layout@version{v.1.1}%
  \PassOptionsToPackage{bugfix.1}{spr-common-flt}}

\DeclareOption{v1.3}{%
  \@version@xitrue%
  \def\fmt@subversion{.3}%
  \def\layout@version{v.1.1}%
  \PassOptionsToPackage{bugfix.1}{spr-common-flt}
  \AtBeginDocument{\setcounter{secnumdepth}{3}}
}

\def\nocorref{\let\corref@text\relax}
\DeclareOption{nocorref}
{\AtEndOfClass{\appto\PROOF\nocorref\appto\CRC\nocorref}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Information about journals

\def\journal@name{Generic Journal}
\def\journal@idd{XXXX}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% process options

\ExecuteOptions{twoside,final,url,bookmarks,outlines,urllinks,pdflinks,nobreaklinks,pagelabels,autorh}
\ProcessOptions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONTS
\def\@xpt{10}
\def\@ixpt{9}
\def\@viiipt{8}

\def\@xipt{11}
\def\@xivpt{14}
\def\@xvipt{16}
\def\@xviiipt{18}
\def\@xxpt{20}
\def\@xxivpt{24}

\def\@ptsize{0}

\if@extendedlayout
  \renewcommand\normalsize{%
     \@setfontsize\normalsize\@xpt{12pt plus .3pt minus .3pt}%
     \abovedisplayskip 10\p@ \@plus2\p@ \@minus2\p@
     \abovedisplayshortskip 6\p@ \@plus2\p@
     \belowdisplayshortskip 6\p@ \@plus2\p@
     \belowdisplayskip \abovedisplayskip}

  \newcommand\small{%
     \@setfontsize\small\@ixpt{11\p@ plus .2\p@ minus .2\p@}%
     \abovedisplayskip 7.5\p@ \@plus4\p@ \@minus1\p@
     \belowdisplayskip \abovedisplayskip
     \abovedisplayshortskip \abovedisplayskip
     \belowdisplayshortskip \abovedisplayskip}

  \newcommand\footnotesize{%
     \@setfontsize\footnotesize\@viiipt{9.5\p@ plus .1pt minus .1pt}%%
     \abovedisplayskip 6\p@ \@plus4\p@ \@minus1\p@
     \belowdisplayskip \abovedisplayskip
     \abovedisplayshortskip \abovedisplayskip
     \belowdisplayshortskip \abovedisplayskip}
\else
  \def\@xpt{9.5}
  \renewcommand\normalsize{%
     \@setfontsize\normalsize\@xpt{11.5pt plus .3pt minus .3pt}%
     \abovedisplayskip 10\p@ \@plus2\p@ \@minus2\p@
     \abovedisplayshortskip 6\p@ \@plus2\p@
     \belowdisplayshortskip 6\p@ \@plus2\p@
     \belowdisplayskip \abovedisplayskip}

  \newcommand\small{%
     \@setfontsize\small\@ixpt{11\p@ plus .2\p@ minus .2\p@}%
     \abovedisplayskip 7.5\p@ \@plus4\p@ \@minus1\p@
     \belowdisplayskip \abovedisplayskip
     \abovedisplayshortskip \abovedisplayskip
     \belowdisplayshortskip \abovedisplayskip}

  \newcommand\footnotesize{%
     \@setfontsize\footnotesize\@viiipt{9.25\p@ plus .1pt minus .1pt}%%
     \abovedisplayskip 6\p@ \@plus4\p@ \@minus1\p@
     \belowdisplayskip \abovedisplayskip
     \abovedisplayshortskip \abovedisplayskip
     \belowdisplayshortskip \abovedisplayskip}
\fi

\newcommand\scriptsize{\@setfontsize\scriptsize\@viipt\@viiipt}
\newcommand\tiny{\@setfontsize\tiny\@vpt\@vipt}
\newcommand\large{\@setfontsize\large\@xiipt{14}}
\newcommand\Large{\@setfontsize\Large\@xivpt{16}}
\newcommand\LARGE{\@setfontsize\LARGE\@xvipt{18}}
\newcommand\huge{\@setfontsize\huge\@xviiipt{20}}
\newcommand\Huge{\@setfontsize\Huge\@xxpt{24}}

% Customization of fonts
\renewcommand\sldefault{it}
\renewcommand\bfdefault{b}
\let\slshape\itshape

% Compatibility with old latex:
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\let\sl\it
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: TEXT 
 
\setlength\parindent    {12\p@}
\def\sv@parindent       {12\p@}
\if@extendedlayout
  \if@version@xi
    \setlength\textheight   {195mm} 
  \else
    \setlength\textheight   {198mm} 
  \fi
  \setlength\textwidth    {338\p@}% 119mm
\else
  \setlength\textheight   {198mm} 
  \setlength\textwidth    {347\p@}% 122mm
\fi
\@settopoint\textheight

%\advance\textheight by\topskip

\setlength\columnsep    {4mm}
\@settopoint\columnsep
\@tempdima=\textwidth
\advance\@tempdima by-\columnsep
\divide\@tempdima by2
\setlength\columnwidth  {\@tempdima}
\@settopoint\columnwidth
\setlength\columnseprule{0\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: HEADS

\setlength\headheight{12\p@}
\setlength\headsep   {14\p@}
\setlength\topskip   {10\p@}
\if@version@xi
  \setlength\footskip  {28\p@}%buvo 17
  \setlength\maxdepth  {.5\topskip}
\else
  \setlength\footskip  {17\p@}%buvo 17
  \setlength\maxdepth  {\z@}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: PAPER AND TRIM SIZE

%% Paper height yra ne 297 kad apgauti distileri HPROOF modoje
\def\paper@width  {210mm}
\def\paper@height {290mm}

\def\trim@width   {155mm}
\def\trim@height  {235mm}

%\def\trim@width   {441.0177pt}
%\def\trim@height  {668.6398pt}

\def\trim@bounding@box{0 0 439.3701 666.1417}

%% jei yra bleed, trim box'as yra pastumiamas per 9bp

\def\set@bleed@box{%
  \if@bleed@box % uzdedame bleed box 9bp
    \def\trim@bounding@box{9 9 448.3701 675.1417}  % +9bp
    \def\bleed@bounding@box{0 0 457.3701 684.1417} % +18bp
%
    \def\write@bleed@box{\special@mt{mt}{destination}{print}{ps:SDict begin [ {ThisPage} << /BleedBox [ \bleed@bounding@box\space] >> /PUT pdfmark  end}}
%
    \advance\voffset by 9bp%
    \advance\hoffset by 9bp%
%
    \def\trim@width {161.350mm} %9bp=3.175mm 18bp = 6.350
    \def\trim@height{241.350mm}
  \fi
}

    
\def\cropmarks@papersize{%
  \let\write@trim@box\relax%
  \let\set@bleed@box\relax%
  \CROPMARKS%
  \AtBeginDocument{\special{papersize=\paper@width, \paper@height}}}

%% cia kablys nustatyti US trim size:
\set@trim@size@hook

% CENTER ON PAGE
\@tempdima=\paper@width \advance\@tempdima by-\trim@width
\divide\@tempdima by2   \advance\@tempdima by-1in
\hoffset=\@tempdima

\@tempdima=\paper@height \advance\@tempdima by-\trim@height
\divide\@tempdima by2    \advance\@tempdima by-1in
\voffset=\@tempdima

% koreguojame
\advance\voffset by-7mm

% PUT ON LEFT CORNER only for the CRC:
\def\proof@papersize{%
\voffset=-1in%
\hoffset=-1in%
\set@bleed@box%
\AtBeginDocument{\if@crop@marks\@latex@error{Kartu nurodytas CRC ir PROOF rezimas}{??}\fi}%
\AtBeginDocument{\special{papersize=\trim@width, \trim@height}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: SIDE MARGINS

\if@extendedlayout
  \setlength\oddsidemargin   {18mm}% gutter margin
  \setlength\evensidemargin  {18mm}% outer
\else
  \setlength\oddsidemargin   {16.5mm}% gutter margin
  \setlength\evensidemargin  {16.5mm}% outer
\fi

\set@margins@hook

\@settopoint\oddsidemargin
\@settopoint\evensidemargin

\addtolength\oddsidemargin {1pt}
\addtolength\evensidemargin{1pt}

\setlength\topmargin       {12mm}
\advance\topmargin by-7pt

\if@twocolumn
 \setlength\marginparsep {10\p@}
\else
  \setlength\marginparsep{7\p@}
\fi
\setlength\marginparpush {5\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: TEXT PARAMETERS

\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\setlength\parskip{0\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: SKIPS

\if@extendedlayout
  \setlength\smallskipamount{6\p@ \@plus 1\p@ \@minus 1\p@}
  \setlength\medskipamount  {12\p@ \@plus 3\p@ \@minus 3\p@}
  \setlength\bigskipamount  {24\p@ \@plus 6\p@ \@minus 3\p@}
\else
  \setlength\smallskipamount{6\p@ \@plus 1\p@ \@minus 1\p@}
  \setlength\medskipamount  {11.5\p@ \@plus 3\p@ \@minus 3\p@}
  \setlength\bigskipamount  {23\p@ \@plus 6\p@ \@minus 3\p@}
\fi

% FOOTNOTES
\setlength\footnotesep   {10\p@}% 7\p@
\setlength{\skip\footins}{18\p@ \@plus 4\p@ \@minus 2\p@}
\skip\@mpfootins = \skip\footins

% FRAMED BOXES
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PAGE-BREAKING PENALTIES

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=50
\predisplaypenalty=10000   % Breaking before a math display.
\pretolerance=100    % Badness tolerance for the first pass (before hyphenation)
\tolerance=800       % Badness tolerance after hyphenation
\hbadness=800        % Badness above which bad hboxes will be shown
\emergencystretch=3\p@
\hfuzz=1\p@           % do not be to critical about boxes


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: COMMON

\if@load@prelims
  \RequirePackage{spr-common-prelims}
\fi

\RequirePackage{pagecrop}

\set@crop@page{\trim@width}{\trim@height} 
\g@addto@macro\HPROOF\cropmarks@papersize
\g@addto@macro\PROOF\proof@papersize
\g@addto@macro\CRC\proof@papersize
\g@addto@macro\HPROOF\@setgridfalse
\g@addto@macro\PROOF\SETGRID
\setgridargs{continues=1}

\def\set@@grid@hook{\if@twocolumn\else\global\let\put@gridnum@right\relax\fi}

\RequirePackage{figlinks}
\RequirePackage[2x]{compose}
\RequirePackage[T1]{fontenc}
\RequirePackage{times}

% Letter sign
% Konfliktas su ammsymb (\Rightarrow) 
%\RequirePackage{marvosym}

% nustatome ar TeXLive 2010, 2012 etc.
% t.y. ar naudojam saka vtex-dist
\newif\if@vtexdist \@vtexdistfalse

% mp 2013-12-30
\@ifundefined{luatexversion}{%
  \@ifundefined{pdftexrevision}{\@vtexdistfalse}{%
    \ifnum\pdftexrevision<11\relax
    \else\@vtexdisttrue
    \fi}
  }{%
  \ifnum\luatexversion<76\relax
  \else\@vtexdisttrue
  \fi}


\if@vtexdist
  \RequirePackage{marvosym}
  \let\@@Letter\Letter
  \def\Letter{\lower.32\p@\hbox{\fontsize{13}{13}\selectfont\@@Letter}\hskip-1.3\p@}
\else
  \DeclareFontFamily{OT1}{mvs}{}
  \DeclareFontShape{OT1}{mvs}{m}{n}{<-> s * [1.45] fmvr8x}{}
  \def\mvs{\usefont{OT1}{mvs}{m}{n}}
  \def\mvchr{\mvs\char}
  \def\Letter{\lower.32\p@\hbox{\mvchr66}\hskip-1.3\p@}
\fi

% letterspacing \dochead (komanda \so)
% \RequirePackage{soul}

% <cmd> <font> <interletter space> <inner-space> <outer-space>
% default: \sodef\so{}{.25em}{.65em\@plus.06em\@minus.08em}{.55em\@plus.12em\@minus.2em}
%\sodef\so{}{.15em}{.39em\@plus.06em\@minus.08em}{.33em\@plus.12em\@minus.2em}


% Helvetica .9 dydzio:
\begingroup
\nfss@catcodes
\input t1phv9x.fd
\endgroup
\RequirePackage{textcomp}
\normalfont\normalsize

% letterspacing atliekamas su specialiu sriftu

\begingroup
\nfss@catcodes
\input t1ptmspac.fd
\endgroup

\def\so#1{{\fontshape{uc070}\selectfont #1}}

\RequirePackage[mtplusscr,mtbold]{mathtime}
\RequirePackage[psamsfonts]{amssymb}
\newcommand\hmmax{0}
\newcommand\bmmax{1}
\RequirePackage{bm}

\if@extendedlayout
\else
  \DeclareMathSizes{9.5}{9.5}{6.5}{5.5}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: FRONT MATTER MACROS

\RequirePackage[journal]{vtexfmpdf}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: url package
\RequirePackage{url}
\urlstyle{same}

\RequirePackage{vtexurl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: LISTS

\RequirePackage{vtexlst}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: SECTIONS

\RequirePackage[normalssect]{vtexsec}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: COLOR

\RequirePackage[dvips]{color}

\definecolor{thirtygray}{gray}{.70}
\definecolor{tengray}{gray}{.90}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: MATH

\RequirePackage{vtexmth}
\if@load@amsmath
  \RequirePackage{vtexamsmath}
  \renewcommand{\big}{\bBigg@{1.1}}%% \big\llbracket is stmaryrd paketo 
  \renewcommand{\Bigg}{\bBigg@{2.6}}
  \allowdisplaybreaks
\fi

% triukas ikisti teksta i numeri:
\def\eqntext#1{\global\let\curr@eqnnum\@eqnnum\gdef\@eqnnum##1##2{$#1$\global\let\@eqnnum\curr@eqnnum}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: THEOREMS

\RequirePackage{timesxi}

\if@load@amsthm
   \RequirePackage{amsthm} 

   \thm@notefont{\upshape}
   \newtheoremstyle{plain}     {\medskipamount}{\medskipamount}{\xishape}{\z@}{\bfseries}{}{1em}{}
   \newtheoremstyle{definition}{\medskipamount}{\medskipamount}{\normalfont}{\z@}{\bfseries}{}{1em}{}
   \newtheoremstyle{remark}    {\medskipamount}{\medskipamount}{\normalfont}{\z@}{\itshape}{}{1em}{}

   \renewenvironment{proof}[1][\proofname]{\par
     \pushQED{\qed}%
     \normalfont \topsep\medskipamount%
     \trivlist
     \labelsep.5em%
     \item[\hskip\labelsep
     \itshape #1\@addpunct{}]\ignorespaces
   }{%
     \popQED\endtrivlist\@endpefalse
   }

\else
   \RequirePackage{vtexthm}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: BIBLIOGRAPHY

\RequirePackage{spr-common-bibl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: SHOWFRONT

\def\show@front{0}

\AtBeginDocument{%
  \ifnum\doc@stage=100\relax
    \ifnum\show@front=1\relax
    \else
      \global\check@lpagefalse
      \RequirePackage[showfront]{verbfron}%
    \fi
  \fi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: sprbibl

\RequirePackage{sprbibl}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: FLOATS

\RequirePackage[rotating]{vtexfltpdf}

\RequirePackage[checkcaption]{spr-common-flt}

\RequirePackage{stfloats}
\fnbelowfloat

% Last page control
\RequirePackage{vslastpage}
\g@addto@macro\PROOF\check@lpagetrue

\RequirePackage{vtexfltcheck}
\g@addto@macro\HPROOF\checkfloats

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: Springer logo

\RequirePackage{springerpi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: full text xml support

\if@load@xml
  \RequirePackage{spr-common-xml}
\fi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FRONT MATTER FORMATTING PARAMETERS


\setattribute{frontmatter} {style} {\raggedright}
\setattribute{address}     {style} {\raggedright}
\setattribute{affiliation} {style} {\raggedright}
\setattribute{abstract}    {style} {\normaltext}
\setattribute{keyword}     {style} {\normaltext\raggedright}

% FRONT MATTER SKIPS
\setattribute{dochead}     {skip} {-\topskip}
\setattribute{title}       {skip} {20\p@}
\setattribute{alttitle}    {skip} {5\p@}
\setattribute{subtitle}    {skip} {5\p@}
\setattribute{altsubtitle} {skip} {5\p@}
\setattribute{authors}     {skip} {23\p@}
\setattribute{copyright}   {skip} {23\p@}
\setattribute{address}     {skip} {4\p@ plus 2\p@}
\setattribute{history}     {skip} {\vfil}
\setattribute{history}  {altskip} {24\p@ plus4pt minus2\p@}
\setattribute{abstract}    {skip} {\medskipamount}
\setattribute{keyword}     {skip} {\medskipamount}
\setattribute{abbr}        {skip} {\medskipamount}

\setattribute{frontmatter} {skip} {\bigskip}
\setattribute{frontmatter} {cmd}  {%
                                  \fmt@motto%
                                  \frontmatter@skip%
                                  \global\@afterindentfalse%
                                  \@afterheading}
\setattribute{title}       {rule} {\hrule height1\p@}

% FRONT MATTER DIMENSIONS
\setattribute{dochead}     {height}{12\p@}
\setattribute{dochead}     {width} {77.5mm}
\setattribute{footnoterule}{width} {108\p@}% 38mm
\setattribute{runninghead} {width} {228\p@}% 80mm
\setattribute{authorminrmargin}{width}{\z@}

% FRONT MATTER FONTS 
\setattribute{dochead}    {size} {\footnotesize\fontshape{uc}\selectfont}
\setattribute{title}      {size} {\fontsize{13pt}{15pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{10mm}}
\setattribute{alttitle}   {size} {\fontsize{11pt}{12pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{subtitle}   {size} {\fontsize{11pt}{12pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{altsubtitle}{size} {\normalsize\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{author}     {size} {\normalsize\bfseries\raggedrightmargin{42mm}}
\setattribute{address}    {size} {\normalsize\itshape}
\setattribute{history}    {size} {\footnotesize\raggedrightmargin{42mm}}
\setattribute{abstract}   {size} {\normalsize}
\setattribute{abstractname}{size} {\bfseries}
\setattribute{keyword}    {size} {\normalsize}
\setattribute{keywordname}{size} {\bfseries}
\setattribute{abbr}       {size} {\normalsize}
\setattribute{abbrname}   {size} {\bfseries}
\setattribute{motto}      {size} {\footnotesize\itshape\raggedright}
\setattribute{fminsert}   {size} {\footnotesize\raggedrightmargin{10mm}}

\setattribute{runninghead}{size} {\footnotesize}
\setattribute{pagenumber} {size} {\footnotesize}
\setattribute{pagenumber}{plainsize} {\footnotesize}
\setattribute{idline}     {size} {\fontsize{6}{6}\selectfont}

\setattribute{copyright}  {size} {\footnotesize}
\setattribute{fmbox}      {size} {66mm}

% caption
\setattribute{caption}{width}{108\p@}% 38mm
\setattribute{caption}{addwidth}{10\p@}% 2mm papild.
\setattribute{caption}{sep}{11\p@}% 4mm

% figure, table fiksuoti dydziai
% medium@width=\textwidth - \caption@width - \caption@sep
\setattribute{small} {width}{108\p@}% 38mm
\setattribute{medium}{width}{227\p@}% 80mm

% TEXT, etc.
\setattribute{corref}         {text} {~\hbox{(\Letter)}}
\setattribute{presentaddress} {text} {\textit{Present address:}\par}
\setattribute{copyright}      {text} {\journal@name\ (\@pubyear)\ \@volume:\@pagerange\\
                                     \@ifundefined{@doi}{}{\nolink@fmt{DOI }{}{\@doi}{\doi@base\@doi}}}
\setattribute{runninghead}    {text} {\journal@name\ (\@pubyear)\ \@volume:\@pagerange}                                 
\setattribute{copyrightline}  {text} {\textcopyright\ \copyrightowner@text\ \@copyrt} 
\setattribute{copyrightowner} {text} {Springer Science+Business Media B.V.}
\setattribute{copyrightlogo}  {text} {\figlink[intext]{springer13}}
\setattribute{onlinelogo}     {text} {}
\setattribute{pdfsubject}     {text} {\journal@name, \volume@label \@volume, \issue@label \@issue, \@pubyear, \@pagerange}
\setattribute{ESMHint}        {text} {Electronic supplementary material}

\setattribute{author}      {sep}  {~{\mathversion{bold}$\cdot$}\ }
\setattribute{author}{addressep}  {~\textperiodcentered\ }
\setattribute{history}     {sep}  {\nobreak~/ }

\setattribute{keyword} {postfix} {\unskip.}

% ISSUE, VOLUME (for the spinoff)

\setattribute{issue} {multilabel} {Nos} 
\setattribute{issue} {twolabel}   {Nos} 
\setattribute{issue} {label}      {No}  

\setattribute{volume} {multilabel} {Volumes} 
\setattribute{volume} {twolabel}   {Volumes} 
\setattribute{volume} {label}      {Volume}  

% HISTORY
\setattribute{received}  {prefix}  {Received:~}
\setattribute{accepted}  {prefix}  {Accepted:~}
\setattribute{revised}   {prefix}  {Revised:~}
\setattribute{registered}{prefix}  {Registered:~}
\setattribute{pubonline} {prefix}  {Published~online:~}

% KEYWORDS
\setattribute{keyword}{AMS}{AMS Subject Classification}
\setattribute{keyword}{MSC}{Mathematics Subject Classification (2000)}
\setattribute{keyword}{MSC2010}{Mathematics Subject Classification (2010)}
\setattribute{keyword}{MSCnoyear}{Mathematics Subject Classification}
\setattribute{keyword}{KWD}{Keywords}
\setattribute{keyword}{JEL}{JEL Classification}
\setattribute{keyword}{PACS}{PACS}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LAYOUT: EXTENDED

\if@extendedlayout

  \setattribute{footnoterule}{width} {105\p@}% 37mm

  \setattribute{caption}{width}{105\p@}% 37mm
  \setattribute{caption}{addwidth}{10\p@}% 2mm papild.
  \setattribute{caption}{sep}{11\p@}% 4mm

  \setattribute{runninghead} {width} {222\p@}% 78mm

  \setattribute{dochead}     {width} {75.5mm}

  % figure, table fiksuoti dydziai
  % medium@width=\textwidth - \caption@width - \caption@sep
  \setattribute{small} {width}{105\p@}% 37mm
  \setattribute{medium}{width}{221\p@}% 78mm

\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  VERSION: v1.1

\if@version@xi
  \setattribute{subtitle}   {size}{\fontsize{11pt}{12pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{10mm}}
  \setattribute{altsubtitle}{size}{\fontsize{11pt}{12pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{10mm}}
  \setattribute{history} {size}{\footnotesize\raggedright}
% pagal specifikacija arnotes desne paraste lygtai 0
  \setattribute{fminsert}{size}{\footnotesize\raggedrightmargin{5mm}}
\fi

\def\check@version@xi{%
  \if@version@xi%
  \else%
    \@latex@error{Nurodykite versija 1.1}{??}
  \fi}

\def\noversioncheck{\let\check@version@xi\relax}

\AtBeginDocument{\check@version@xi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  LAYOUT: altlayout 
% (for articles with main language german)

\if@altlayout

  \setattribute{alttitle}{skip}    {13\p@}
  \setattribute{alttitle}{size}    {\fontsize{10pt}{12.5pt}\selectfont\bfseries\mathversion{bold}\raggedright}
  \setattribute{altsubtitle}{size} {\fontsize{10pt}{12.5pt}\selectfont\bfseries\mathversion{bold}\raggedright}

\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-fm

\RequirePackage{spr-common-fm}

%% fmbox - nededame i deze

\newif\if@fmbox \@fmboxfalse
\def\fmbox{%
  \vtop to\fmbox@size\bgroup
  \@fmboxtrue
  \hsize=\textwidth
  }

\def\endfmbox{\if@history\else{\history@skip\history@size\par\copyrightline@text\par\vskip\copyright@skip}\fi\par \egroup%
  \@ifundefined{@runtitle}{}{\markboth{\@runtitle}{\@runtitle}}%
  \@ifundefined{@runauthor}{}{\markleft{\@runauthor}}}%


%% title 

\def\title@fmt[#1]#2{%
  \vglue\dochead@skip
  \title@rule
  \vbox to\dochead@height{\@ifundefined{@dochead}{}{\colorbox{thirtygray}{\hbox to\dochead@width{\dochead@size\@dochead\hfill}}}\par\vfill}
  \vskip\title@skip%
  \setkeys{title}{#1}%
  \bgroup%
    \no@harm%
    \let\protect\relax%
    \pdfstringdef\@argi{#2}%
  \egroup%
  \write@pdfinfo{\hy@title}{\@argi}
  \bgroup
    \set@rh@hook{runtitle}{#2}%
    \PREHOOK@title@fmt{#2}%
    \title@size\csname pretitle@text\endcsname #2\par%
  \egroup}

%%%% fminsert

\long\def\fminsert#1{%
  \gdef\footnoterule{\global\let\footnoterule\orig@footnoterule}
  \insert\footins{%
     \fminsert@size
     \setlength\smallskipamount{5\p@}
     \setlength\medskipamount{10\p@}
     \vskip2.7\p@
     \hrule width\footnoterule@width
     \vskip4.2pt
    #1\par}}

%%%% printaddresses

\let\printaddresses\fminsert

%%%% tikriname ar autoriu sarasas > 3 eil.: tada reikia naudoti opcija [wide]

\g@addto@macro\HPROOF\set@checkaug
\g@addto@macro\PROOF\set@checkaug

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-body

\RequirePackage{spr-common-body}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROOF, CRC SETTINGS: PROOF@INFO
% inormacijos RH numusimas PROOF stadijoje:

\def\proof@info{
  \AtBeginDocument{%
  \if@acid
  \else
    \setattribute{copyright}{text}  {\journal@name\\\@ifundefined{@doi}{}{\nolink@fmt{DOI }{}{\@doi}{\doi@base\@doi}}}
    \setattribute{runninghead}{text}{\journal@name}
    \setattribute{pdfsubject} {text}{\journal@name - uncorrected proof}
    \let\thepage@fmt\@gobble
  \fi}}

\g@addto@macro\PROOF\proof@info
\g@addto@macro\HPROOF\proof@info

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROOF, CRC SETTINGS: \springeronline

\def\springeronline{}
\def\springerproof{\g@addto@macro\headline@hook\journal@idline}

\def\set@proof{%
  \let\headline@hook\set@watermark%
  \gdef\@id@line{\hfill}%
  \gdef\@id@dim@@{\hfill}%
  \gdef\sgml@info{\hfill}}

\def\crc@settings{%
  \check@addr%
  \nokwdcheck%
  \noartcheck%
  \global\@setgridfalse%
  \let\journal@idline\relax%
  \let\check@rh@hook\@gobble%
  \let\headline@hook\relax%
  \hidecites%
  \hideuncited}

% Isvedame informacine eilute HPROOF ir PROOF modose
\g@addto@macro\HPROOF\springerproof
\g@addto@macro\PROOF\springerproof

\g@addto@macro\springeronline\crc@settings
\g@addto@macro\CRC\crc@settings

% \springeronline versijoje isvedame dummy logo
\g@addto@macro\springeronline\set@dummy@logo
\g@addto@macro\CRC\clear@dummy@logo


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-pdfsettings

\RequirePackage[journalsettings]{spr-common-pdfsettings}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INICIALIZATION

\@twosidetrue
\pagenumbering{arabic}
\frenchspacing
\pagestyle{headings}


\endinput
%%
%% End of file `spr-small-v1.cls'.
