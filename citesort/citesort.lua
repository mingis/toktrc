------------------------------------------------------------------------------
-- citesort.lua
--
--    Nustato counterį \vtex@cite@pos einamosios pagrindinio failo pozicijos
--          reikšme. Naudojama failo *.aux \citation įrašų rikiavimui.
--
--    Scenarijus aktyvuojamas toktrc papildyto LuaTex kompiliatoriaus
--          iškvietimo komandoje pridėjus parinktį --lua=citesort.lua:
--
--          luatex.exe --fmt=lualatex --progname=latex --lua=citesort.lua foo.tex
--
--   2013  Mindaugas Piešina, mindaugas.piesina@vtex.lt
--
-- Changelog:
--   2013-12-03  mp  pradinis sukūrimas
--

-----------------
-- toktrc.cfg parseris
require("tokinit")


------------------------------------------------------------------------
-- "top_token_tracer" callbacko funkcijos paprogramė
-- @param tok  eilinis įvedimo srauto tokenas
------------------------------------------------------------------------
local function token_tracer_callback(tok)

    local cur_lnum = get_cur_lnum()   -- einamosios input failo eilutės nr.
    local cur_lpos = get_cur_lpos()   -- einamosios input failo eilutės pozicija

    tex.count['vtex@cite@pos'] = 100000000 + cur_lnum * 1000 + cur_lpos
end


------------------------------------------------------------------------
-- "stop_run" callback'as
------------------------------------------------------------------------
local function stop_run_callback()

    -- skaitom .aux failą
    local aux_fname = cfgt.default.mainfilename
    -- numetam prievardį „.tex“, jeigu yra
    local fname, ftype = aux_fname:match('(.*)%.([^%.]*)');
    if fname then aux_fname = fname end
    aux_fname = aux_fname .. '.aux'

    local aux_file = io.open(aux_fname, "r")

    -- visos .aux failo eilutės
    local aux_lines = {}
    local aux_lnum = 0

    -- \citation eilutės rikiavimui
    local cit_recs = {}
    local cit_rnum = 0

    -- \styledata [type={cite}, ... eilutės rikiavimui
    local stdat_recs = {}
    local stdat_rnum = 0

    for aux_line in aux_file:lines() do
        aux_lnum = aux_lnum + 1
        aux_lines[aux_lnum] = aux_line

        local cit_par, cit_ord = aux_line:match('\\citation{(.*)}{(.*)}')
        if (cit_par) then
            cit_rnum = cit_rnum + 1
            -- Prie rikiavimo indekso galo prilipdom \citation eilės numerį.
            -- Perrikiuoti reikia tik skirtingų .tex failo eilučių sugeneruotus
            -- \citation, tos pačios eilutės \citation eilės tvarka gera, o
            -- \vtex@cite@pos dažnai stovi vietoj (pvz., \caption pradžioj).
            -- Jeigu paliksim, kaip yra, to paties \vtex@cite@pos \citation'ai
            -- susimaišys. Pridedam prieky raidę, kad rikiavimas lygintų
            -- kaip tekstą – skaičiai jau virš 32 bitų, gali būt problemų.
            cit_recs[cit_rnum] = {'X' .. cit_ord ..
                                            (100000 + cit_rnum), cit_par}
        end

        local stdat_line, stdat_par, stdat_ord = aux_line:match('\\styledata %[type={cite},inputline={(.*)},bibitems=(.*),ix={(.*)}%]')
        if (stdat_par) then
            stdat_rnum = stdat_rnum + 1
            -- Prie rikiavimo indekso galo prilipdom \citation eilės numerį.
            stdat_recs[stdat_rnum] = {'X' .. stdat_ord ..
                                        (100000 + stdat_rnum), stdat_par}
        end
    end

    aux_file:close();

    -- rikiuojam
    table.sort(cit_recs, function(a, b) return a[1] < b[1] end)
    table.sort(stdat_recs, function(a, b) return a[1] < b[1] end)

    -- rašom atgal, jau be rikiavimo indeksų
    aux_file = io.open(aux_fname, "w")

    cit_rnum = 0
    stdat_rnum = 0

    -- dubletų naikinimui
    local cit_written = {}
    local stdat_written = {}

    for ix, aux_line in pairs(aux_lines) do
        local cit_par, cit_ord = aux_line:match('\\citation{(.*)}{(.*)}')
        if (cit_par) then
            cit_rnum = cit_rnum + 1
            -- X100196014100000 - eil. nr. ištraukiam iš 100196
            local lnum = string.sub(cit_recs[cit_rnum][1], 2, 7) - 100000
            -- tikrinam, ar ne dubliuotas
            local ix_str = lnum .. "/" .. cit_recs[cit_rnum][2]
            if not cit_written[ix_str] then
                cit_written[ix_str] = true
                aux_file:write("\\citation{" .. cit_recs[cit_rnum][2] .. "}\n")
            end
        else
            local stdat_par, stdat_ord = aux_line:match('\\styledata %[type={cite},(.*),ix={(.*)}%]')
            if (stdat_par) then
                stdat_rnum = stdat_rnum + 1
                -- eil. nr.
                local lnum = string.sub(stdat_recs[stdat_rnum][1], 2, 7) - 100000
                -- tikrinam, ar ne dubliuotas
                local ix_str = lnum .. "/" .. stdat_recs[stdat_rnum][2]
                if not stdat_written[ix_str] then
                    stdat_written[ix_str] = true
                    aux_file:write("\\styledata [type={cite},inputline={" ..
                        lnum .. "},bibitems=" .. stdat_recs[stdat_rnum][2] ..
                        "]\n")
                end
            else
                aux_file:write(aux_line .. "\n")
            end
        end
    end

    aux_file:close();

end


----------------------------------------------------
-- pradžia, inicializacija

--------------------------------------
-- hookinam eventus
callback.register("top_token_tracer", token_tracer_callback)
callback.register("stop_run", stop_run_callback)
