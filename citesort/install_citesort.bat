set instpath=%1
if "%instpath%"=="" set instpath=.

set binpath=%instpath%\win32

call %~dp0..\common\win32\install_toktrc_common.bat %instpath%

copy %~dp0citesort.bat %instpath%
copy %~dp0citesort.lua %instpath%
copy %~dp0vtexnatbib.cfg %instpath%
copy %~dp0vtexnatbib.sty %instpath%
copy %~dp0citedbg.sty %instpath%
copy %~dp0spr-twocol-v1.cls %instpath%
copy %~dp0spr-small-v1.cls %instpath%
