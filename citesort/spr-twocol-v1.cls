% SPRINGER
%   Global Layout Specification:  
%   Medium
%   Large
%
% TeX programming: Vytas Statulevicius, VTeX, Lithuania, vytas@vtex.lt
% Requires Latex2e, ver.2000.06
%
%
% Problemos:
% - wide footnote dideliu adresu formavimui
% - equations per abi koloneles (zr. linijas)
% - figure* fulwidth caption turi buti per 2 koloneles jei eil.nr>2
%
% 2006.11.22 - started
% id="MedLaySpec"    ver. 1.1.2  2006.03.03
% id="LayoutJA102"   ver. 1.2.3  2006.05.29
%
% 2006.12.11 - i id eilute irasytas layout tekstas
%            - paper@height nustatytas 290mm
% 2006.12.12 - atjungtas marvosymb paketo krovimas
% 2006.12.13 - komanda \url neformuoja link'o
% 2006.12.14 - ivesta komanda \tabnotesinline;
%            - sutvarkyta sidewaystable
%            - sutvarkyta klaida thm aplinkose kai numeravimas priklauso nuo section
%            - PROOF modeoje isvedamas id per RH mechanizma
%            - ijungtas citesort
%            - ideti raktai \ead[fax]{}, ead[phone]{}
% 2006.12.15 - i id eilute idetas journal@number
% 2006.12.22 - ivesta aplinka {biography}
% 2006.12.23 - ivestas raktas presentaddressref
%            - pakeisti trim width ir trim height (zr. laiska)
% 2006.12.28 - sutvarkyta history
% 2007.01.03 - ivestas raktas nocorref: \printauthor[nocorref={au1,au2}]{au1,au2}
% 2007.01.09 - pries accepted, pubonline dedamas " / "
% 2007.01.10 - timessi pakeistas i timesxi (+ status {}[]0-9)
% 2007.01.11 - ideta \postbox komanda
% 2007.01.12 - tabnotes numeruojamos a,b.. + sumazintas tarpas pries firsttabnote
%            - pakeistas \figurename
% 2007.01.16 - \ignorespaces prie \endtabular pridedamas su (AtBeginDocument (kad veiktu su array.sty)
%            - i {ack}, {acks} {artnote} idetas \par gale
%            - ivesta komanda \newremark
%            - ivesta komanda \revised{}
% 2007.01.17 - tikrinama ar \author komandoje yra nuoroda (addressref=) i \address (id=)
%            - tikriname ar biography aplinkos authorref yra egzistuojantis + jis isvedamas parasteje HPROOF modoje
% 2007.01.19 - prijugtas palaikymas "nobreaklinks" (nedarome linku uztempimo)
% 2007.01.22 - pataisyta klaida su \cite spalvinimu;
%            - \springeronline isjungia cite spalvinima
%            - nuimtas : DOI; pakeista MSC ;
% 2007.01.23 - ivesti keyword raktai JEL, PACS
%            - amsthm modoje pataisyta remark
%            - idetas tikrinimas ar author, address id yra unikalus
%            - journal info padidintas sriftas
%            - ijungtas raktas nobreaklinks
% 2007.01.25 - is \noharm isimtas \let~\space
%            - sutvarkytas \contcaption{}
% 2007.01.26 - pakeistas figurecaption@fmt
%            - atstatytas MSC
% 2007.01.29 - showuncited isveda <unc> tiny sriftu
%            - pakeistas \journal@info sriftas
% 2007.01.31 - nameyear atveju cite@sep pakeistas i ";"
%            - \setlength\maxdepth  {.5\topskip} pakeista i 0pt
% 2007.02.01 - sutvarkytas biography (gale \par)
% 2007.02.02 - idetas lastpage tikrinimas PROOF modoje
% 2007.02.05 - paragraphi po numerio nededa tasko
% 2007.02.06 - ideta online aplinka ir \CopyrightHolder
% 2007.02.12 - ivesta komanda \printaug[]{au1,au2}{aff1}{e1,e2}
%            - ivesta komanda \bioauthorname
%            - ivesti raktai auprefix, addrprefix
%            - keyval paketas netikrina raktu apibreztumo
% 2007.02.13 - pataisyta \check@aug
%            - idetas \smart@par
% 2007.02.15 - pakeistas ESMHint@text
% 2007.02.20 - present address isvedamas pries autoriaus pavarde;
%            - pakeistas ESMHint@text
%            - pakeista frontmatter@cmd
%            - pataisytas formules atitraukimas list'e (vsfleqn2.sty)
% 2007.02.21 - pataisytas bugas presentaddressref
%            - \fminsert apibreztas kaip long
%            - pakeistas journal@info
% 2007.02.22 - jei nera ead id: \printaug{au1}{aff}{} corref atributai nuvalomi
%            - tikriname ar visi \ead buvo isvesti su \printead (\check@ead)
%            - ivesta komanda \doiurl{http://dx.doi.org/...} arba \doiurl{...}
%            - \printaug: jei nei vienas autoriu nera corresponding ir autoriu>1 po adreso email'as nespausdinamas
%            - \springeronline ijungia \hideuncited
%            - number literaturos atveju \cite gali buti tik: \cite[Petrov \byear{1999}]{r1}
%            - raktas noeadcheck (jei nera email'u!)
% 2007.02.23 - ivesti kontroles raktai standardrh, optionalrh
%            - sumazintas \journal@info sriftas
%            - idetos komados \cyr ir \textcyr
% 2007.02.26 - idetas raktas norhcheck
% 2007.02.28 - pataisyta sidewaystable
%            - ideta komanda \corrigendum
% 2007.03.03 - padidintas zenkliukas \letter
%            - \dochead ijungtas letterspacing (SOUL)
% 2007.03.04 - klase suskaidyta i spr-common-fm.sty, spr-common-flt.sty, spr-common-body.sty
%            - ivestas \authorminrmargin@width
%            - ivestas \runninghead@width
% 2007.03.05 - padidintas zenkliukas \letter
%            - sumazintas interletter spacing (SOUL) 
% 2007.03.06 - pajungtas paketas multicol
% 2007.03.06 - idetas \g@addto@macro\CRC\springeronline
% 2007.03.07 - idetas raktas noartcheck
% 2007.03.09 - nuimtas prefiksas "~/ " is accepted@prefix etc.
%            - pataisyta Large klaida \raggedrightmargin is 84 pakeista i 90
% 2007.03.14 - link'u spalva nustatyta i [0 0 1]
%            - idetas \TrimBox
% 2007.03.15 - prijungtas paketas spr-common-prelims
%            - pataisyta \cite[] number atveju
% 2007.03.27 - idetas surisimas Published~online
% 2007.03.29 - ideta opcija cuted (paketo cuted pakrovimas)
% 2007.04.05 - ideti raktai: kwdrequired, jelrequired, mscrequired, pacsrequired
%            - nokwdcheck - tikrinimo isjungimas
% 2007.04.21 - springeronline modoje ir CRC ijungtas \nokwdcheck
% 2007.04.26 - ideta opcija nooutlines (isjungia bookmarku generavima)
%            - idetos opcijos artlangcheck, noartlangcheck (ijungiama zurnalams kuriems privaloma
%              komanda \artlang{}
% 2007.05.04 - ivestas raktas noaugcheck (netikrina corref)
% 2007.05.14 - class=MSC idetas \allowbreak
% 2007.06.05 - ivesta opcija v1.1 (stiliaus parametrai atitinka spec. Medium v.1.3, 2007.01.23;
%              Large v.1.3, 2007.01.23)       
%            - pataisytas amsmath 
% 2007.09.25 - pakeistas ESMHint@text
% 2007.10.02 - pakeistas abstract@skip (buvo 0pt)
% 2007.10.17 - amsmath opcijoje idetas \allowdisplaybreaks
% 2007.11.13 - idetas "kablys" onlinelogo@text
% 2008.01.04 - ivestas raktas noartlangcheck
% 2008.01.16 - idetas parametras title@rule
% 2008.01.29 - ivestas raktas nospinoffcheck
% 2008.02.01 - nustatyti issue@label, volume@label (naudojama spinoff)
% 2008.02.06 - \special{papersize=...} perkeltas is AtBeginDvi i AtBeginDocument
% 2008.03.28 - ijungti isoriniai link'ai
% 2008.04.01 - isjungti isoriniai link'ai
% 2008.04.11 - prijungtas paketas vtexurl
% 2008.04.14 - nuimtas grid HPROOF modoje
% 2008.04.16 - ijungti isoriniai link'ai
% 2008.04.26 - nuo straipsnio doi neformuojamas link'as
% 2008.05.09 - ijungtas raktas autobibl
% 2008.05.27 - pataisyta klaida su raktu nobookmarks
% 2008.08.07 - ijungtas tikrinimas \check@addr
% 2008.11.10 - pakeista id eilute
% 2008.11.14 - \springeronline versijoje isvedame dummy logo
% 2008.11.17 - istaisyta klaida del \springeronline
% 2009.09.17 - ivesta opcija bleedbox (spalvotu puslapiu spausdinimui)
% 2009.11.12 - teksto deze paslinkta per 5 punktus zemyn (keista headsep ir tt.)
% 2009.11.15 - ivesta versija 1.2 ir raktas \noversioncheck
% 2009.11.17 - v1.2 pataisymas del prelims konflikto
% 2009.11.21 - nustatyti alttitle ir altsubtitle parametrai
%            - ivesta opcija altlayout
% 2009.12.04 - pakeistas \alttitle altlayout modoje
% 2009.12.17 - prijungtas paketas spr-common-pdfsettings
% 2010.02.11 - bendros options perkeltos i spr-common-options stiliu
% 2010.04.14 - if@alttitle perkelta po versiju tikslinimo
% 2010.09.01 - isjungtas \marginparsep
% 2010.09.23 - ivestos opcijos MSC2010, MSCnoyear
% 2010.10.11 - letterspacing padaytas specialaus srifto pagalba
% 2010.12.08 - ivesta komandos \acid{} palaikymas
% 2010.12.15 - pataisytas \acid
% 2011.01.18 - pataisytas "medium" dochead plotis
% 2011.01.21 - prijungtas vtexfltcheck (neveikia dblfloat'ams)
%            - lastpage pakeistas vslastpage
% 2011.05.18 - pataisytas copyright@text
% 2011.05.19 - pataisytas runninghead@text
% 2011.08.16 - ivesta opcija v1.3
% 2011.10.11 - kraunamas paketas spr-common-xml
% 2011.12.23 - ivesta opcija v1.4
% 2012.01.10 - ideta opcija checkcaption
% 2012.01.13 - pakeista t1ptmspac.fd 
% 2012.01.19 - pakeistas altsubtitle sriftas
% 2012.01.24 - corref zenkliukas idetas i \hbox
% 2012.02.28 - ideta \renewcommand{\big}{\bBigg@{1.1}}
% 2012.11.28 - Live 2010 dist. kitaip marvosym paketas
% 2012.11.28 - pataisyta latex006 formatui ()
% 2012.12.04 - tikriname ar TeX Live 2010 distr. ir tada kraunam marvosym paketa
% 2012.12.21 - ijungtas raktas autorh
% 2013.03.14 - bibliografija iskelta i spr-common-bibl
% 2013.04.04 - idetas tikrinimas ar autoriai uzima >3 eil. \check@aug@height (OTRS 17214)

\def\fmt@name{spr-twocol-v1}
\def\fmt@version{2013/04/04}

\NeedsTeXFormat{LaTeX2e}[2000/06/01]
\ProvidesClass{spr-twocol-v1}
              [\fmt@version, Springer Medium/Large layout]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% load package with common options and definitions

\RequirePackage{spr-common-options}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - layout
% Layout options
\def\layout@version{v.1.1}
\def\layout@text{Medium \layout@version}
\newif\if@largelayout \@largelayoutfalse

\DeclareOption{large}{\def\layout@text{Large \layout@version}\@largelayouttrue}

% Switch for layout
\newif\if@small@layout
\@small@layoutfalse

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - class versios
% version options
\newif\if@version@xi \@version@xifalse

\DeclareOption{v1.1}{%
   \@version@xitrue
   \def\fmt@subversion{.1}
   \def\layout@version{v.1.3.1}}

\newif\if@version@xii \@version@xiifalse

\DeclareOption{v1.2}{%
   \@version@xitrue
   \@version@xiitrue
   \def\fmt@subversion{.2}
   \def\layout@version{v.1.3.2}}

\DeclareOption{v1.3}{%
   \@version@xitrue
   \@version@xiitrue
   \def\fmt@subversion{.3}
   \def\layout@version{v.1.3.2}
   \PassOptionsToPackage{bugfix.1}{spr-common-flt}}

\DeclareOption{v1.4}{%
   \@version@xitrue
   \@version@xiitrue
   \def\fmt@subversion{.4}
   \def\layout@version{v.1.3.2}
   \PassOptionsToPackage{bugfix.1}{spr-common-flt}
   \AtBeginDocument{\setcounter{secnumdepth}{3}}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% class specific options - load packages
% Load cuted:
\newif\if@load@cuted \@load@cutedfalse  
\DeclareOption{cuted}{\@load@cutedtrue}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Information about journals

\def\journal@name{Generic Journal}
\def\journal@idd{XXXX}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% process options

\ExecuteOptions{twocolumn,twoside,final,url,bookmarks,outlines,urllinks,pdflinks,nobreaklinks,pagelabels,autorh}
\ProcessOptions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FONTS

\def\@xpt{10}
\def\@ixpt{9}
\def\@viiipt{8}

\def\@xipt{11}
\def\@xivpt{14}
\def\@xvipt{16}
\def\@xviiipt{18}
\def\@xxpt{20}
\def\@xxivpt{24}

\def\@viiipt{8.5\p@}

\def\@ptsize{0}

\renewcommand\normalsize{%
   \@setfontsize\normalsize\@xpt{12.5\p@ plus .3\p@ minus .3\p@}%
   \abovedisplayskip 10\p@ \@plus2\p@ \@minus2\p@
   \abovedisplayshortskip 6\p@ \@plus2\p@
   \belowdisplayshortskip 6\p@ \@plus2\p@
   \belowdisplayskip \abovedisplayskip}

\newcommand\small{%
   \@setfontsize\small\@ixpt{11\p@ plus .2\p@ minus .2\p@}%
   \abovedisplayskip 7.5\p@ \@plus4\p@ \@minus1\p@
   \belowdisplayskip \abovedisplayskip
   \abovedisplayshortskip \abovedisplayskip
   \belowdisplayshortskip \abovedisplayskip}

\newcommand\footnotesize{%
   \@setfontsize\footnotesize\@viiipt{10\p@ plus .1pt minus .1pt}%%
   \abovedisplayskip 6\p@ \@plus4\p@ \@minus1\p@
   \belowdisplayskip \abovedisplayskip
   \abovedisplayshortskip \abovedisplayskip
   \belowdisplayshortskip \abovedisplayskip}

\newcommand\scriptsize{\@setfontsize\scriptsize\@viipt\@viiipt}
\newcommand\tiny{\@setfontsize\tiny\@vpt\@vipt}
\newcommand\large{\@setfontsize\large\@xiipt{14}}
\newcommand\Large{\@setfontsize\Large\@xivpt{16}}
\newcommand\LARGE{\@setfontsize\LARGE\@xvipt{18}}
\newcommand\huge{\@setfontsize\huge\@xviiipt{20}}
\newcommand\Huge{\@setfontsize\Huge\@xxpt{24}}

% Customization of fonts
\renewcommand\sldefault{it}
\renewcommand\bfdefault{b}
\let\slshape\itshape

% Compatibility with old latex:
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\let\sl\it
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: TEXT 

\setlength\parindent    {12\p@}
\def\sv@parindent       {12\p@}
\if@largelayout
  \setlength\textwidth    {495\p@}% 174mm
  \setlength\textheight   {234mm} 
  \setlength\columnwidth  {239\p@}% 84mm
  \setlength\columnsep    {17\p@}%  6mm
\else
  \setlength\textwidth    {455\p@}% 160mm
  \setlength\textheight   {216mm} 
  \setlength\columnwidth  {216\p@}% 76mm
  \setlength\columnsep    {23\p@}%  8mm
\fi
\@settopoint\textheight


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: HEADS

\setlength\headheight{12\p@}
\setlength\headsep   {9\p@}
\setlength\footskip  {30\p@}
\setlength\topskip   {10\p@}
\setlength\maxdepth  {\z@}

\if@version@xii
   \setlength\headsep   {14\p@} %2009.11.12 - buvo 9pt
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: PAPER AND TRIM SIZE
%% Paper height yra ne 297 kad apgauti distileri HPROOF modoje
\def\paper@width  {210mm}
\def\paper@height {290mm}

\if@largelayout
  \def\trim@width   {210mm}
  \def\trim@height  {279mm}
  \def\trim@bounding@box{0 0 595.2756 790.8661}
\else
  \def\trim@width   {193mm}
  \def\trim@height  {260mm}
  \def\trim@bounding@box{0 0 547.0866 737.0079}% bp!
\fi

%% jei yra bleed, trim box'as yra pastumiamas per 9bp

\def\set@bleed@box{%
  \if@bleed@box % uzdedame bleed box 9bp
    \if@largelayout%
       \set@bleed@box@large%
    \else%
       \set@bleed@box@medium%
    \fi%
  \fi%
}


\def\set@bleed@box@large{%
%  \def\trim@bounding@box{0 0 595.2756 790.8661}
   \def\trim@bounding@box{9 9 604.2756 799.8661}  % +9bp
   \def\bleed@bounding@box{0 0 613.2756 808.8661} % +18bp
%
   \def\write@bleed@box{\special@mt{mt}{destination}{print}{ps:SDict begin [ {ThisPage} << /BleedBox [ \bleed@bounding@box\space] >> /PUT pdfmark  end}}
%
   \advance\voffset by 9bp%
   \advance\hoffset by 9bp%
%
   \def\trim@width   {216.350mm} % 18bp = 6.350
   \def\trim@height  {285.350mm}
}


\def\set@bleed@box@medium{%
% \def\trim@bounding@box{0 0 547.0866 737.0079}% bp!
  \def\trim@bounding@box{9 9 556.0866 746.0079}% +9bp
  \def\bleed@bounding@box{0 0 565.0866 755.0079}% +18bp 
%
  \def\write@bleed@box{\special@mt{mt}{destination}{print}{ps:SDict begin [ {ThisPage} << /BleedBox [ \bleed@bounding@box\space] >> /PUT pdfmark  end}}
%
   \advance\voffset by 9bp%
   \advance\hoffset by 9bp%
%
  \def\trim@width   {199.350mm} % 18bp = 6.350mm
  \def\trim@height  {266.350mm}
}


\def\cropmarks@papersize{%
  \let\write@trim@box\relax%
  \let\set@bleed@box\relax%
  \CROPMARKS%
  \AtBeginDocument{\special{papersize=\paper@width, \paper@height}}}

% CENTER ON PAGE
\@tempdima=\paper@width \advance\@tempdima by-\trim@width
\divide\@tempdima by2   \advance\@tempdima by-1in
\hoffset=\@tempdima

\@tempdima=\paper@height \advance\@tempdima by-\trim@height
\divide\@tempdima by2    \advance\@tempdima by-1in
\voffset=\@tempdima

% koreguojame
\advance\voffset by7mm

% PUT ON LEFT CORNER only for the CRC:
\def\proof@papersize{%
\voffset=-1in%
\hoffset=-1in%
\set@bleed@box%
\AtBeginDocument{\if@crop@marks\@latex@error{Kartu nurodytas CRC ir PROOF rezimas}{??}\fi}%
\AtBeginDocument{\special{papersize=\trim@width, \trim@height}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: SIDE MARGINS

\if@largelayout
  \setlength\oddsidemargin   {18mm}% gutter margin
  \setlength\evensidemargin  {18mm}% outer
\else
  \setlength\oddsidemargin   {16.5mm}% gutter margin
  \setlength\evensidemargin  {16.5mm}% outer
\fi

\@settopoint\oddsidemargin
\@settopoint\evensidemargin

\setlength\topmargin       {12mm}
\advance\topmargin by-7pt

\if@twocolumn
 \setlength\marginparsep {10\p@}
\else
  \setlength\marginparsep{7\p@}
\fi
\setlength\marginparpush {5\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: TEXT PARAMETERS

\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\setlength\parskip{0\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONS: SKIPS

\setlength\smallskipamount{6\p@ \@plus 1\p@ \@minus 1\p@}
\setlength\medskipamount  {12.5\p@ \@plus 3\p@ \@minus 3\p@}
\setlength\bigskipamount  {25\p@ \@plus 6\p@ \@minus 3\p@}

% FOOTNOTES
\setlength\footnotesep   {10\p@}% 7\p@
\setlength{\skip\footins}{18\p@ \@plus 4\p@ \@minus 2\p@}
\skip\@mpfootins = \skip\footins

% FRAMED BOXES
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PAGE-BREAKING PENALTIES

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=50
\predisplaypenalty=10000   % Breaking before a math display.
\pretolerance=100    % Badness tolerance for the first pass (before hyphenation)
\tolerance=800       % Badness tolerance after hyphenation
\hbadness=800        % Badness above which bad hboxes will be shown
\emergencystretch=3\p@
\hfuzz=1\p@           % do not be to critical about boxes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: COMMON

\if@load@prelims
  \RequirePackage{spr-common-prelims}
\fi

\RequirePackage{pagecrop}

\set@crop@page{\trim@width}{\trim@height} 

\g@addto@macro\HPROOF\cropmarks@papersize
\g@addto@macro\PROOF\proof@papersize
\g@addto@macro\CRC\proof@papersize
\g@addto@macro\HPROOF\@setgridfalse
\g@addto@macro\PROOF\SETGRID
\setgridargs{continues=1}

\def\set@@grid@hook{\if@twocolumn\else\global\let\put@gridnum@right\relax\fi}

\RequirePackage{figlinks}
\RequirePackage[2x]{compose}
\RequirePackage[T1]{fontenc}
\RequirePackage{times}

% nustatome ar TeXLive 2010, 2012 etc.
% t.y. ar naudojam saka vtex-dist
\newif\if@vtexdist \@vtexdistfalse

% mp 2013-12-12
\@ifundefined{luatexversion}{%
  \@ifundefined{pdftexrevision}{\@vtexdistfalse}{%
    \ifnum\pdftexrevision<11\relax
    \else\@vtexdisttrue
    \fi}
  }{%
  \ifnum\luatexversion<76\relax
  \else\@vtexdisttrue
  \fi}

\if@vtexdist
  \RequirePackage{marvosym}
  \let\@@Letter\Letter
  \def\Letter{\lower.35\p@\hbox{\fontsize{12.5}{12.5}\selectfont\@@Letter}\hskip-1.4\p@}
\else
  \DeclareFontFamily{OT1}{mvs}{}
  \DeclareFontShape{OT1}{mvs}{m}{n}{<-> s * [1.47] fmvr8x}{}
  \def\mvs{\usefont{OT1}{mvs}{m}{n}}
  \def\mvchr{\mvs\char}
  \def\Letter{\lower.35\p@\hbox{\mvchr66}\hskip-1.4\p@}
\fi

% letterspacing \dochead (komanda \so)
%\RequirePackage{soul}

% <cmd> <font> <interletter space> <inner-space> <outer-space>
% default: \sodef\so{}{.25em}{.65em\@plus.06em\@minus.08em}{.55em\@plus.12em\@minus.2em}
%\sodef\so{}{.15em}{.39em\@plus.06em\@minus.08em}{.33em\@plus.12em\@minus.2em}

% Helvetica .9 dydzio:
\begingroup
\nfss@catcodes
\input t1phv9x.fd
\endgroup
\RequirePackage{textcomp}
\normalfont\normalsize

% letterspacing atliekamas su specialiu sriftu

\begingroup
\nfss@catcodes
\input t1ptmspac.fd
\endgroup

\def\so#1{{\fontshape{uc070}\selectfont #1}}

\RequirePackage[mtplusscr,mtbold]{mathtime}
\RequirePackage[psamsfonts]{amssymb}
\newcommand\hmmax{0}
\newcommand\bmmax{1}
\RequirePackage{bm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: FRONT MATTER MACROS

\RequirePackage[journal]{vtexfmpdf}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: url package

\RequirePackage{url}
\urlstyle{same}

\RequirePackage{vtexurl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: LISTS

\RequirePackage{vtexlst}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: SECTIONS

\RequirePackage[normalssect]{vtexsec}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: COLOR

\RequirePackage[dvips]{color}
\definecolor{thirtygray}{gray}{.70}
\definecolor{tengray}{gray}{.90}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: MATH

\RequirePackage{vtexmth}


\RequirePackage{vsfleqn2}
\AtEndOfClass{\mathindent\z@}

% Pataisom kad atsitrauktu list'e:

\def\mathtrivlist{\parsep\parskip\topsep\abovedisplayskip
%  \ifnum\@listdepth>0 \advance\mathindent by-\leftmargin\fi%
  \@trivlist \labelwidth\z@ \leftmargin\z@
  \itemindent\z@ \def\makelabel##1{##1}
}

\if@load@amsmath
  \RequirePackage{vtexamsmath}
  \renewcommand{\big}{\bBigg@{1.1}}%% \big\llbracket is stmaryrd paketo 
  \renewcommand{\Bigg}{\bBigg@{2.6}}
  \allowdisplaybreaks
\fi

% triukas ikisti teksta i numeri:
\def\eqntext#1{\global\let\curr@eqnnum\@eqnnum\gdef\@eqnnum##1##2{$#1$\global\let\@eqnnum\curr@eqnnum}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: THEOREMS

\RequirePackage{timesxi}

\if@load@amsthm
   \RequirePackage{amsthm} 

   \thm@notefont{\upshape}
   \newtheoremstyle{plain}     {\medskipamount}{\medskipamount}{\xishape}{\z@}{\bfseries}{}{1em}{}
   \newtheoremstyle{definition}{\medskipamount}{\medskipamount}{\normalfont}{\z@}{\bfseries}{}{1em}{}
   \newtheoremstyle{remark}    {\medskipamount}{\medskipamount}{\normalfont}{\z@}{\itshape}{}{1em}{}

   \renewenvironment{proof}[1][\proofname]{\par
     \pushQED{\qed}%
     \normalfont \topsep\medskipamount%
     \trivlist
     \labelsep.5em%
     \item[\hskip\labelsep
     \itshape #1\@addpunct{}]\ignorespaces
   }{%
     \popQED\endtrivlist\@endpefalse
   }

\else
   \RequirePackage{vtexthm}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: BIBLIOGRAPHY

\RequirePackage{spr-common-bibl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: SHOWFRONT

\def\show@front{0}

\AtBeginDocument{%
  \ifnum\doc@stage=100\relax
    \ifnum\show@front=1\relax
    \else
      \global\check@lpagefalse
      \RequirePackage[showfront]{verbfron}%
    \fi
  \fi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: sprbibl

\RequirePackage{sprbibl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: FLOATS

\RequirePackage[rotating]{vtexfltpdf}

\RequirePackage[checkcaption]{spr-common-flt}

\if@twocolumn
  \RequirePackage{multicol}%
  \RequirePackage{flushend}%
  \RequirePackage{stfloats}%
  \fnbelowfloat%
  \if@load@cuted
     \RequirePackage[autobase]{cuted}%
  \fi
\fi
                                                                                                             
% Last page control
\RequirePackage{vslastpage}
\g@addto@macro\PROOF\check@lpagetrue

\RequirePackage{vtexfltcheck}

\g@addto@macro\HPROOF\checkfloats

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: Springer logo

\RequirePackage{springerpi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: full text xml support

\if@load@xml
  \RequirePackage{spr-common-xml}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FRONT MATTER FORMATTING PARAMETERS

\setattribute{frontmatter} {style} {\raggedright}
\setattribute{abstract}    {style} {\normaltext}
\setattribute{keyword}     {style} {\normaltext\raggedright}

% FRONT MATTER SKIPS
\setattribute{pretitle}    {skip} {12\p@}
\setattribute{title}       {skip} {21\p@}
\setattribute{alttitle}    {skip} {18\p@}
\setattribute{subtitle}    {skip} {18\p@}
\setattribute{altsubtitle} {skip} {18\p@}
\setattribute{authors}     {skip} {17\p@}
\setattribute{copyright}   {skip} {19\p@}
\setattribute{address}     {skip} {4\p@ plus 2\p@}
\setattribute{history}     {skip} {\vfil}
\setattribute{history}  {altskip} {24\p@ plus4pt minus2\p@}
\setattribute{abstract}    {skip} {\medskipamount}
\setattribute{keyword}     {skip} {\medskipamount}
\setattribute{abbr}        {skip} {\medskipamount}
\setattribute{frontmatter} {cmd}  {%
                                  \fmt@motto%
                                  \bigskip%
                                  \global\@afterindentfalse%
                                  \@afterheading}
\setattribute{title}       {rule} {\hrule height1\p@}

% FRONT MATTER DIMENSIONS
\setattribute{dochead}     {height}{19\p@}
\setattribute{dochead}     {width} {72mm}
\setattribute{footnoterule}{width} {45\p@}% 16mm
\setattribute{runninghead} {width} {347\p@}% 122mm
\setattribute{authorminrmargin}{width}{38mm}% medium: max author size:122mm. 160-122=38mm

% FRONT MATTER FONTS 
\setattribute{dochead}    {size} {\fontsize{9.5pt}{9.5pt}\fontshape{uc}\selectfont}
\setattribute{title}      {size} {\fontsize{18pt}{18pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{alttitle}   {size} {\fontsize{14pt}{14pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{subtitle}   {size} {\fontsize{14pt}{14pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{altsubtitle}{size} {\normalsize\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
\setattribute{author}     {size} {\normalsize\bfseries\raggedrightmargin{84mm}}% 76+8=84
\setattribute{address}    {size} {\footnotesize\raggedrightmargin{5mm}}
\setattribute{history}    {size} {\footnotesize\raggedright}
\setattribute{abstract}   {size} {\normalsize}
\setattribute{abstractname}{size} {\bfseries}
\setattribute{keyword}    {size} {\normalsize\raggedright}
\setattribute{keywordname}{size} {\bfseries}
\setattribute{abbr}       {size} {\normalsize}
\setattribute{abbrname}   {size} {\bfseries}
\setattribute{motto}      {size} {\footnotesize\itshape\raggedright}
\setattribute{fminsert}   {size} {\footnotesize\raggedright}

\setattribute{runninghead}{size} {\footnotesize}
\setattribute{pagenumber} {size} {\footnotesize}
\setattribute{pagenumber}{plainsize} {\footnotesize}
\setattribute{idline}     {size} {\fontsize{6}{6}\selectfont}

\setattribute{copyright}  {size} {\footnotesize}
\setattribute{fmbox}      {size} {82mm}

% caption
\setattribute{caption}{width}{96\p@}% 34mm
\setattribute{caption}{addwidth}{10\p@}% 2mm papild.
\setattribute{caption}{sep}{23\p@}% 8mm

% figure, table fiksuoti dydziai
% medium@width=\textwidth - \caption@width - \caption@sep
\setattribute{small} {width}{96\p@}% 34mm
\setattribute{medium}{width}{335\p@}% 118mm

% TEXT, etc.
\setattribute{corref}         {text} {~\hbox{(\Letter)}}
\setattribute{presentaddress} {text} {\textit{Present address:}\par}
\setattribute{copyright}      {text} {\journal@name\ (\@pubyear)\ \@volume:\@pagerange\\
                                     \@ifundefined{@doi}{}{\nolink@fmt{DOI }{}{\@doi}{\doi@base\@doi}}}
\setattribute{runninghead}    {text} {\journal@name\ (\@pubyear)\ \@volume:\@pagerange}                                 
\setattribute{copyrightline}  {text} {\textcopyright\ \copyrightowner@text\ \@copyrt} 
\setattribute{copyrightowner} {text} {Springer Science+Business Media B.V.}
\setattribute{copyrightlogo}  {text} {\figlink[intext]{springer13}}
\setattribute{onlinelogo}     {text} {}
\setattribute{pdfsubject}     {text} {\journal@name, \volume@label \@volume, \issue@label \@issue, \@pubyear, \@pagerange}
\setattribute{ESMHint}        {text} {Electronic supplementary material}

\setattribute{author}      {sep}  {~{\mathversion{bold}$\cdot$}\ }
\setattribute{author}{addressep}  {~\textperiodcentered\ }
\setattribute{history}     {sep}  {\nobreak~/ }

\setattribute{keyword} {postfix} {\unskip.}

% ISSUE, VOLUME (for the spinoff)

\setattribute{issue} {multilabel} {Nos} 
\setattribute{issue} {twolabel}   {Nos} 
\setattribute{issue} {label}      {No}  

\setattribute{volume} {multilabel} {Volumes} 
\setattribute{volume} {twolabel}   {Volumes} 
\setattribute{volume} {label}      {Volume}  

% HISTORY
\setattribute{received}  {prefix}  {Received:~}
\setattribute{accepted}  {prefix}  {Accepted:~}
\setattribute{revised}   {prefix}  {Revised:~}
\setattribute{registered}{prefix}  {Registered:~}
\setattribute{pubonline} {prefix}  {Published~online:~}

% KEYWORDS
\setattribute{keyword}{AMS}{AMS Subject Classification}
\setattribute{keyword}{MSC}{Mathematics Subject Classification (2000)\allowbreak}
\setattribute{keyword}{MSC2010}{Mathematics Subject Classification (2010)\allowbreak}
\setattribute{keyword}{MSCnoyear}{Mathematics Subject Classification\allowbreak}
\setattribute{keyword}{KWD}{Keywords}
\setattribute{keyword}{JEL}{JEL Classification}
\setattribute{keyword}{PACS}{PACS}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LAYOUT: LARGE

\if@largelayout

  \setattribute{idline}     {size} {\fontsize{7}{7}\selectfont}

  \setattribute{dochead}     {width} {79.5mm}
% author
  \setattribute{author}{size} {\normalsize\bfseries\raggedrightmargin{90mm}}% 84+6=90
  \setattribute{authorminrmargin}{width}{45mm}% large max author size:129mm. 174-129=45mm
% caption
  \setattribute{caption}{width}{111\p@}% 39mm
  \setattribute{caption}{addwidth}{10\p@}% 2mm papild.
  \setattribute{caption}{sep}{17\p@}% 6mm

  \setattribute{runninghead}{width}{367\p@}% 129mm

% figure, table fiksuoti dydziai
% medium@width=\textwidth - \caption@width - \caption@sep
  \setattribute{small} {width}{110\p@}% 39mm
  \setattribute{medium}{width}{366\p@}% 118mm

\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  VERSION: v1.1

\if@version@xi
  \if@largelayout
  \else
    \setattribute{dochead}{width}{71.5mm}
  \fi
  \setattribute{title}   {size}{\fontsize{16pt}{18pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
  \setattribute{alttitle}{size}{\fontsize{12pt}{14pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
  \setattribute{subtitle}{size}{\fontsize{12pt}{14pt}\selectfont\bfseries\mathversion{bold}\raggedrightmargin{15mm}}
  \setattribute{footnoterule}{width} {51\p@}% 18mm
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  VERSION: v1.2


\if@version@xii
   \setattribute{pretitle}{skip} {7\p@} %2009.11.12 - buvo 12pt
   \setattribute{fmbox}   {size} {80mm} % 2009.11.12 - buvo 82mm 5pt=1.75mm
\fi


\def\check@version@xii{%
  \if@version@xii%
  \else%
    \@latex@error{Nurodykite versija 1.2}{??}
  \fi}

\def\noversioncheck{\let\check@version@xii\relax}

\AtBeginDocument{\check@version@xii}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  LAYOUT: altlayout 

\if@altlayout

  \setattribute{alttitle}    {skip} {13\p@}
  \setattribute{altsubtitle} {skip} {5\p@}
  \setattribute{alttitle}    {size} {\fontsize{10pt}{12.5pt}\selectfont\bfseries\mathversion{bold}\raggedright}
  \setattribute{altsubtitle} {size} {\fontsize{10pt}{12.5pt}\selectfont\bfseries\mathversion{bold}\raggedright}

\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-fm

\RequirePackage{spr-common-fm}

%%%% tikriname ar autoriu sarasas > 3 eil.: tada reikia naudoti opcija [wide]

\g@addto@macro\HPROOF\set@checkaug
\g@addto@macro\PROOF\set@checkaug

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-body

\RequirePackage{spr-common-body}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROOF, CRC SETTINGS: PROOF@INFO
% inormacijos RH numusimas PROOF stadijoje:

\def\proof@info{
  \AtBeginDocument{%
  \if@acid
  \else
    \setattribute{copyright}{text}  {\journal@name\\\@ifundefined{@doi}{}{\nolink@fmt{DOI }{}{\@doi}{\doi@base\@doi}}}
    \setattribute{runninghead}{text}{\journal@name}
    \setattribute{pdfsubject} {text}{\journal@name - uncorrected proof}
    \let\thepage@fmt\@gobble
  \fi}}

\g@addto@macro\PROOF\proof@info
\g@addto@macro\HPROOF\proof@info

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROOF, CRC SETTINGS: \springeronline

\def\springeronline{}
\def\springerproof{\let\headline@hook\journal@idline}

\def\crc@settings{%
  \check@addr%
  \nokwdcheck%
  \noartcheck%
  \noversioncheck
  \global\@setgridfalse%
  \let\journal@idline\relax%
  \let\check@rh@hook\@gobble%
  \let\headline@hook\relax%
  \hidecites%
  \hideuncited}

% Isvedame informacine eilute HPROOF ir PROOF modose
\g@addto@macro\HPROOF\springerproof
\g@addto@macro\PROOF\springerproof

\g@addto@macro\springeronline\crc@settings
\g@addto@macro\CRC\crc@settings

% \springeronline versijoje isvedame dummy logo
\g@addto@macro\springeronline\set@dummy@logo
\g@addto@macro\CRC\clear@dummy@logo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOADING PACKAGES: spr-common-pdfsettings

\RequirePackage[journalsettings]{spr-common-pdfsettings}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INICIALIZATION

\@twosidetrue
\pagenumbering{arabic}
\frenchspacing
\pagestyle{headings}

\endinput
%%
%% End of file `spr-twocol-v1.cls'.
