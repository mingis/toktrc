------------------------------------------------------------------------------
-- file_tracer.lua
--      luatex skaitomų failų trasavimas
--      bandymas išsiaiškinti, ar standartiniai "open_read_file" ir "process_input_buffer" callback'ai galėtų pakeisti toktrc
--          ne -- failo neužtenka, reikia ir pozicijos faile -- be jos nepavyks atskirti, ar tokenas iš pagrindinio failo, ar išskleistas iš stiliaus
--
--      Scenarijus aktyvuojamas LuaTex kompiliatoriaus iškvietimo komandoje
--          pridėjus parinktį --lua=file_tracer.lua
--      Pvz.:
--          luatex.exe --fmt=lualatex --progname=latex --lua=file_tracer.lua foo.tex
--
-- 2017  Mindaugas Piešina, mindaugas.piesina@vtex.lt
--
-- Changelog:
--      2017-05-18  mp  initial migration from /home/mingis/F/kp/src/vtex/tex/toktrc/pvz/lua_pavyzdys/line_tracer.lua
--      2017-05-19  mp  tracing file name in reader()
--
------------------------------------------------------------------------------

---------------------------------------------------------------
-- "open_read_file" calback'o funkcija
-- gaudo pagrindinio failo atidarymą
-- @param filename  perduodamas failo pavadinimas (string))
-- @return {reader, close} -- aprašymai prie pačių funkcijų apibrėžimų
---------------------------------------------------------------
function catch_input_file(filename)

    logfile:write('catch_input_file(): open file: ' .. filename .. '\n')

    if not filename or filename == "" then
        return empty_file
    end

    local chan = io.open(filename, "r")

    if not chan then
        return empty_file
    end

    -------------------------------------------------
    -- Funkcija iškviečiama kiekvienos esamo failo
    -- input eilutės pradžioje
    -- @return line  nuskaityta failo eilutė
    -- treisina skaitomo failo vardą
    -------------------------------------------------
    local reader = function()
        logfile:write('reader(): ' .. filename .. '\n')
        line = chan:read("*l")
        return line
    end

    -------------------------------------------------
    -- Funkcija iškviečiama uždarinėjant failą
    -- @return nil
    -------------------------------------------------
    local close = function ()
        chan:close()
    end

    return {reader = reader, close = close}
end

-------------------------------
-- Darbo pabaigos callback'as
-------------------------------
function stop_run_callback()
    logfile:write("stop_run_callback()")
    logfile:close()
end

-------------------------------
-- Iniciavimo procedūros
-------------------------------
logfile = io.open("file_tracer.log", "w")
logfile:write("init")

callback.register("open_read_file", catch_input_file)
callback.register("stop_run", stop_run_callback)
