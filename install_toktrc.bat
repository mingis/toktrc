set instpath=%1
if "%instpath%"=="" set instpath=.

call %~dp0common\win32\install_toktrc_common.bat %instpath%

:: copy %~dp0toktrc_win32.sty %instpath%
:: copy %~dp0toktrc.inc %instpath%
copy %~dp0toktrc.lua %instpath%

copy %~dp0toktrc.py %instpath%
copy %~dp0toktrc3.py %instpath%

mkdir %instpath%\kppylib
copy %~dp0kppylib\__init__.py %instpath%\kppylib
mkdir %instpath%\kppylib\common
copy %~dp0kppylib\common\__init__.py %instpath%\kppylib\common
copy %~dp0kppylib\common\kpcapp.py %instpath%\kppylib\common
copy %~dp0kppylib\common\kpstdio.py %instpath%\kppylib\common
mkdir %instpath%\kppylib\env
copy %~dp0kppylib\env\__init__.py %instpath%\kppylib\env
mkdir %instpath%\kppylib\env\common
copy %~dp0kppylib\env\common\__init__.py %instpath%\kppylib\env\common
copy %~dp0kppylib\env\common\kperr.py %instpath%\kppylib\env\common
mkdir %instpath%\conf
copy %~dp0conf\__init__.py %instpath%\conf

copy %~dp0conf\config.ini %instpath%\conf
copy %~dp0config.ini %instpath%

copy %~dp0toktrc_pavyzdys.py %instpath%
copy %~dp0toktrc3_pavyzdys.py %instpath%

copy %~dp0toktrc.bat %instpath%
copy %~dp0toktrc_py.bat %instpath%
