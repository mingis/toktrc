#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
chkerr.py

    Tikrina TeX kompiliavimo klaidas

Syntax:
    python chkerr.py foo.log
"""

import sys, re, ctypes

# --------------------------------
log_file = open(sys.argv[1], 'r')

log_lines = log_file.readlines()

msg_lines = []
for line in log_lines:
    if (re.search(r'!(.*)', line) or re.search(r'LaTeX Warning:(.*)', line)):
        if (not re.search(r'Non-PDF special ignored!(.*)', line)):
            msg_lines.append(line)
log_file.close()

while (len(msg_lines) > 20):
    msg_lines[0] = "....\n"
    del msg_lines[1]

if (len(msg_lines)):
    ctypes.windll.user32.MessageBoxW(0, "".join(msg_lines), "Yra klaidų", 1)
