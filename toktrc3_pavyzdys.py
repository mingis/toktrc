#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
toktrc_pavyzdys.py
    Python interfeisas su luatex toktrc.inc tokenų treiseriu.
    
    Failo toktrc.py naudojimo pavyzdys.

Changelog:
    2013-10-07  mp  initial creation    
    2013-11-14  mp  python33
    2013-11-14  mp  lowercase toktrc.cfg keys and values

"""

import sys, traceback
from toktrc3 import TokTrc

# ---------------------------------
class TokTrcTest(TokTrc):

    def __init__(self, distribution, file_path, all_calls=False, CWD=None):
        super(TokTrcTest, self).__init__(distribution, file_path, all_calls, CWD)

    def get_ms_stat(self, lnum, lpos):
            self.scan_log_file() # iš *.tokrc.log formuojamas stat_recs[]

            # ieškom artimiausio
            nearest_dist = sys.maxsize
            nearest_ix = -1

            for ii in range(len(self.stat_recs)):
                dist = abs(int(self.stat_recs[ii]['lpos']) - lpos) + 1000 * \
                    abs(int(self.stat_recs[ii]['lnum']) - lnum)
                if (dist < nearest_dist):
                    nearest_dist = dist
                    nearest_ix = ii

            assert(nearest_ix >= 0)

            ret_val = self.stat_recs[nearest_ix]
            ret_val['diff'] = nearest_dist

            return ret_val

    def print_res(self, ret_vals):
        print("{}:{} {}  grplev: {}  mathmode: {}  ixlev: {}  environment: {}  diff: {}".format(
            ret_vals["lnum"], ret_vals["lpos"], ret_vals["text"],
            ret_vals["grplev"], ret_vals["mathmode"], ret_vals["ixlev"],
            ret_vals["environment"], ret_vals["diff"]))
        assert ret_vals["diff"] == 0, "Token position not found"


# ---------------------------------
# paprastas TokTrc.get_ms_stat() iškvietimas – nurodytos pozicijos grupės 
#   gylio sužinojimas
print(TokTrcTest("2010", "be_pavyzdys.tex", False, "tmp3").get_ms_stat(49, 36)["grplev"])

# ---------------------------------
# TokTrc.get_ms_stat() iškvietimas kelioms pozicijoms su klaidų analize
#       perduodam all_calls == True, kad formuotų pilnus failus *.used.log
#       ir *.unused.log
try:
    toktrc_obj = TokTrcTest("2010", "be_pavyzdys.tex", True, "tmp3")
  
    ret_vals = toktrc_obj.get_ms_stat(49, 4)
    toktrc_obj.print_res(ret_vals)

    ret_vals = toktrc_obj.get_ms_stat(67, 10)
    toktrc_obj.print_res(ret_vals)

    ret_vals = toktrc_obj.get_ms_stat(51, 56)
    toktrc_obj.print_res(ret_vals)

except Exception as exc:
    print("\n! Error:", exc)
    tback_lines = traceback.format_exc().split("\n")
    for ii in range(1, len(tback_lines) - 3):
        if (ii % 2 == 1):
            print(tback_lines[ii])
