:: ----------------------
set distribution=2010
set branch=vtex
set texroot=D:\texroot
set texlive=texlive%distribution%
set tlroot=%texroot%\%texlive%
set binpath=%tlroot%\bin\win32
set scriptpath=%texroot%\bat
set libpath=%binpath%

:: ----------------------
:: negalima naudoti --draftmode – neiškvies "stop_run" callbacko
:: call vtex %distribution% luatex.exe --fmt=lualatex --progname=latex --recorder --interaction=nonstopmode --lua=toktrc.lua %1 > %1.log 2>&1

:: ----------------------
set path=%~dp0common\win32;%binpath%;%scriptpath%;%libpath%;%path%

set TEXMFCNF=%tlroot%\texmf-%branch%\web2c

set TEXMFLOG=%branch%_tex_dvi.log
if exist "%TEXMFLOG%" del "%TEXMFLOG%"

::set LUAINPUTS=%~dp0
::set LUA_PATH=%~dp0configparser.lua
copy %~dp0configparser.lua %~dp1
copy %~dp0tokinit.lua %~dp1

if exist "toktrc.cfg" goto skip_cfg
copy %~dp0toktrc.cfg %~dp1
echo mainfilename = %1 >> %~dp1toktrc.cfg
:skip_cfg

:: %binpath%\luatex.exe --fmt=lualatex --progname=latex --recorder --interaction=nonstopmode --lua=toktrc.lua %1 > %1.log 2>&1
luatex.exe --fmt=lualatex --progname=latex --recorder --interaction=batchmode --lua=%~dp0toktrc.lua %1

:: del %~dp1configparser.lua
:: del %~dp1tokinit.lua
