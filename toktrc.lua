------------------------------------------------------------------------------
-- toktrc.lua
--    LuaTex tokenų skanavimo trasavimo įrankis
--        darbiniam aplanke pagamina failą foo.tex.toktrc.log arba toktrc.log
--        naudojamas perdarytas LuaTex kompiliatorius
--
--    Scenarijus aktyvuojamas LuaTex kompiliatoriaus iškvietimo komandoje
--        pridėjus parinktį --lua=toktrc.lua:
--
--        luatex.exe --fmt=lualatex --progname=latex --lua=toktrc.lua foo.tex
--
--   2013  Mindaugas Piešina, mindaugas.piesina@vtex.lt
--
-- Changelog:
--   2013-03-06  mp  initial implementation as toktrc.inc
--   2013-05-27  mp  implemented get_cur_*()
--   2013-05-29  mp  implemented as toktrc.sty; \currfilename
--   2013-09-20  mp  implementation of toktrc.cfg
--   2013-09-23  mp  back to toktrc.inc; no \currfilename
--   2013-09-23  mp  trace_stops implemented  
--   2013-09-24  mp  grp_lev implemented
--   2013-09-24  mp  ix_levs implemented
--   2013-09-24  mp  current font trace
--   2013-09-24  mp  trace page number
--   2013-09-25  mp  panaudotų makrokomandų sąrašas names_used
--   2013-09-27  mp  math_mode implemented
--   2013-09-27  mp  panaudotas tex.primitives 
--   2013-09-27  mp  kaupiami \csname ... \endcsname vardai
--   2013-09-30  mp  tex.currentgrouplevel and tex.nest[tex.nest.ptr].mode
--   2013-09-30  mp  names_used kaupiamas tik einamojo failo ribose
--   2013-10-04  mp  top_token_tracer
--   2013-10-07  mp  IxLev delimiter from ":" to "/"
--   2013-10-07  mp  TraceFont = Full
--   2013-10-09  mp  toktrc.lua
--   2013-10-11  mp  TraceLevel = Tracer/Filter; AllTokens = Calls
--   2013-10-29  mp  Environment, TraceEnv, trace_env ir get_exp_str()
--   2013-11-08  mp  long lines split, comments added
--   2013-11-08  mp  local variables and functions
--   2013-11-08  mp  texio.write_nl()
--   2013-11-11  lr, mp  ConfigParser implemented
--   2013-11-11  mp  format_ix_lev_str()
--   2013-11-11  mp  ChkLpos removed
--   2013-11-11  mp  removed TraceGrpLev = Internal
--   2013-11-11  mp  removed TraceMathMode = Internal
--   2013-11-13  mp  toktrc.cfg parameter ExpandMacros, output column Expansion
--   2013-11-14  mp  lowercase toktrc.cfg keys and values
--   2013-11-25  mp  stilių failų vardai į *.used.log
--   2013-12-04  mp  valdančių simbolių konvertavimas prieš csname išvedimą
--

-----------------
-- tex tokenų konstantos
require "tokinit"

-----------------
-- „globalių“ kintamųjų ir konstantų inicializavimas

-- log failų priesagos
local toktrc_log_fsuff = "toktrc.log"
local used_log_fsuff = "used.log"
local unused_log_fsuff = "unused.log"
local undefined_log_fsuff = "undefined.log"

---------------
-- „globalūs“ kintamieji

-- visų į token.get_next() atėjusių tokenų skaitiklis
local tokcnt = 0

-- patreisintų tokenų skaitiklis
local listed_tokcnt = 0

-- puslapių skaitiklis
local cur_page = 1

-- paskutinė skanavimo būsena
local last_cur_lnum = 0   -- einamosios input failo eilutės nr.
local last_cur_lpos = 0   -- einamosios input failo eilutės pozicija

local prev_tcmd = relax_cmd   -- pradinė reikšmė prieš pirmą tokeną – \relax,
                        -- čia math mode junginėjimui, names_used[] ir
                        --     names_defined[] kaupimui

-- grupės gylio skaitiklis
local cur_grp_lev = 0 -- iš tex.currentgrouplevel

-- math mode skaitiklis: 0 – tekstinis režimas, 1 – inline matematika,
--     2 – pastraipinė matematika
local math_mode = 0       -- mano suskaičiuotas pagal math mode switch-us
                    --     skanuojamų tokenų sraute 
local cur_mode = 0        -- iš tex.nest[tex.nest.ptr].mode
-- cur_math_mode = 0 -- iš tex.nest[tex.nest.ptr].mathmode, visada nil

-- sub/super-skripto indeksų lygmenų masyvas
-- kiekvienam gilesniam indeksui kuriamas naujas ix_levs elementas,
-- grįžus iš indekso, paskutinis lygmuo naikinamas
-- kiekvienas elementas susideda dar iš dviejų:
-- ix_levs[ii][1] – indekso lygmuo, superskriptų teigiamas, subskriptų
--                      neigiamas,
-- ix_levs[ii][2] – indekso pradžios cur_grp_lev reikšmė, į
--                      ją grįžus, indekso lygmenį naikinam
local ix_levs = {}
local num_of_ix_levs = 0

-- \csname lygmuo: 0 – \csname ... \endcsname išorė
--                 1 – \csname vidus, kaupiam csname_val, prieš tai
--                   buvo \def ar co, prijunginėsim prie names_defined
--                 2 – \csname vidus, kaupiam csname_val, prieš tai
--                   \def ar co nebuvo, prijunginėsim prie names_used
local csname_lev = 0

-- kaupiamas \csname ... \endcsname tokeno vardas
local csname_val = ""

-- makrokomandų vardų masyvai
local names_used = {} -- hash'as:
                --      indeksai – panaudotų macro vardai su 
                --          prilipdytais tokenų identifikatoriais tid,
                --          kad grupės viduj apibrėžtos makrokomandos
                --          būtų traktuojamos atskirai
                --      reikšmės – masyvai iš dviejų elementų:
                --          names_used["xxx"][1] – macro vardas,
                --          names_used["xxx"][2] – macro tokeno
                --              identifikatorius tid
local names_defined = {} -- apibrėžtų vardų hashas, struktūra kaip names_used

------------------------------------------------------------------------
-- "token_filter" ar "top_token_tracer" callbacko funkcijos paprogramė;
--                  čia vyksta pagrindinė tokenų srauto analizė ir
--                  rezultato failo išvedimas
-- @param tok   tokeno trijų elementų table, grąžinta iš token.get_next()
--                  arba gauta kaip "top_token_tracer" parametras
-- @return tok  grąžinamas tas pats gautas argumentas tok, tiesiog
--                  dėl tvarkos -- "token_filter" callback'as iš
--                  principo tokenus gali konvertuoti; čia šitos
--                  galimybės nenaudojam dėl suderinamumo su
--                  "top_token_tracer" callback'u
------------------------------------------------------------------------
local function token_callback(tok)

    local tcmd = tok[1]     -- tokeno komandos kodas
                            -- command code; this is a value between
                            --      0 and 130 (approximately)

    local tchcode = tok[2]  -- tokeno simbolio kodas
                            -- command modifier; this is a value
                            --      between 0 and 221

    local tid = tok[3]      -- tokeno identifikatorius   
                            -- control sequence id; for commands, that are
                            --      not the result of control sequences,
                            --      like letters and characters, it is zero,
                            --      otherwise, it is a number pointing into
                            --      the ‘equivalence table’

    local cmdname = token.command_name(tok) -- tokeno komandos vardas

    local csname = token.csname_name(tok)   -- tokeno vardas

    -- "_" -- tokeno vardas su identifikatorium, 
    -- naudojamas hash'ų  names_defined[] ir names_used[] indeksams
    local csname_tid = csname .. underscore_chr .. tid

    local cur_fname = get_cur_fname() -- einamojo input failo vardas
    local cur_lnum = get_cur_lnum()   -- einamosios input failo eilutės nr.
    local cur_lpos = get_cur_lpos()   -- einamosios input failo eilutės pozicija

    tokcnt = tokcnt + 1     -- tokenų skaitiklis

    ------------------
    -- group level
    cur_grp_lev = tex.currentgrouplevel

    ------------------
    -- math mode
    if (cfgt.default.tracemathmode)
    then
        cur_mode = tex.nest[tex.nest.ptr].mode
        if      (cur_mode == nil) then      cur_mode = 0 end

        if      (cur_mode <= -233) then     cur_mode = 1
        elseif  (cur_mode >= 233) then      cur_mode = 2
        else                                cur_mode = 0
        end

        -- if (cur_mode == math_mode) then cur_mode = "" end

        -- visada nil
        -- cur_math_mode = tex.nest[tex.nest.ptr].mathmode
        -- if (cur_math_mode == nil) then cur_math_mode = "" end

        math_mode = cur_mode

    end -- if (cfgt.default.tracemathmode)

    --------------------- 
    -- kaupiam used ir defined

    -------------------- 
    -- kaupiam \csname ... \endcsname tokeno vardą

    -- išorėje if (proceed) šakos, nes turim pagauti ir \csname viduj
    --      esančius makrokomandų skleidimus
    if (csname_lev) and
        ((tcmd == letter_cmd) or (tcmd == other_char_cmd))
    then
            csname_val = csname_val .. string.char(tchcode)
    end

    -------------------- 
    -- šitas gabalas buvo išvedimo viduj -- nepagaunam,
    --      kai iškvietimas apibrėžtas makrokomandos viduj

    --------------
    -- kaupiam panaudotus vardus
    if (
        ( -- call, long_call, outer_call, long_outer_call
            (tcmd == call_cmd) or (tcmd == long_call_cmd) or
            (tcmd == outer_call_cmd) or (tcmd == long_outer_call_cmd)
        ) and
        (not 
            (
                (prev_tcmd == let_cmd) or   -- \let \futurelet
                (prev_tcmd == def_cmd) or   -- \def \edef \gdef \xdef
                (prev_tcmd == shorthand_def_cmd)    -- \chardef \countdef
            )   -- ignoruojam perapibrėžimo atvejus
        )
       )
    then
        if (not names_used[csname_tid])
        then
            names_used[csname_tid] = {csname, tid}
        end
    end

    ---------------- 
    -- surinkinėjam \csname tokeno vardą paraidžiui tarpe tarp
    --      \csname ir \endcsname
     
    -- šitoks \csname vardo surinkimas neveiks, pvz., tokiais atvejais:
    --      \expandafter\def%
    --          \csname\expandafter\@gobbletwo\string\if@restonecol\endcsname
    --               (čia pagausim raides "if@restonecol")
    --      \expandafter\def%
    --          \csname\expandafter\@gobbletwo\string\iffalse\endcsname
    --               (čia pagausim raides "iffalse")
    --      rezultatas bus "if@restonecoliffalse", o reikia,
    --          matyt, "@restonecolfalse"
    --
    if (tcmd == cs_name_cmd) -- cs_name \csname
    then
        if (not (csname_lev == 0))
        then
            texio.write_nl("! toktrc.lua: unbalanced " .. bslash_chr ..
                "csname ... " .. bslash_chr .. "endcsname pairs")
        end

        if (
            (prev_tcmd == let_cmd) or   -- \let \futurelet
            (prev_tcmd == def_cmd) or   -- \def \edef \gdef \xdef
            (prev_tcmd == shorthand_def_cmd) -- \chardef \countdef
           )
        then
            csname_lev = 1 -- apibrėžimas
        else  
            csname_lev = 2 -- iškvietimas
        end
        csname_val = ""
    end

    if (tcmd == end_cs_name_cmd) -- end_cs_name \endcsname
    then
        local tid_val = token.csname_id(csname_val)
        local csname_val_tid = csname_val .. underscore_chr .. tid_val -- "_"

        if (csname_lev == 1)
        then
            if (not names_defined[csname_val_tid])
            then
                names_defined[csname_val_tid] =
                    {csname_val, tid_val, cur_fname}
            end
        elseif (csname_lev == 2)
        then
            if (not names_used[csname_val_tid])
            then
                names_used[csname_val_tid] = {csname_val, tid_val}
            end
        else
            texio.write_nl("! toktrc.lua: unbalanced " .. bslash_chr ..
                "csname ... " .. bslash_chr .. "endcsname pairs")
        end
        csname_val = ""
        csname_lev = 0
    end

    --------------
    -- papildomai kaupiam apibrėžiamus vardus
    -- tex.hashtokens() kažkodėl neatspindi visų apibrėžtų
    --       be_pavyzdys.tex:
    --           \def\beee{\end{equation}}
    --           \def\bbeee{\end{equation}}
    --       į tex.hashtokens() įtraukiami, o 
    --           \def\eee{\end{equation}}
    --       ne
    if (
        (
            (tcmd == undefined_cs_cmd) or   -- undefined_cs; tikėtina,
                                            --      prieš tai buvo \def
            (prev_tcmd == let_cmd) or   -- \let \futurelet
            (prev_tcmd == def_cmd) or   -- \def \edef \gdef \xdef
            (prev_tcmd == shorthand_def_cmd) -- \chardef \countdef
        ) and
        (not (tcmd == cs_name_cmd))
       )
    then
        if (not names_defined[csname_tid])
        then
            names_defined[csname_tid] = {csname, tid, cur_fname}
        end
    end

    -------------------------------------------------
    -- tikrinam, ar reikia išvedinėti
    local proceed = false

    --------------- 
    -- tikrinam mainfilename
    -- cfgt.default.mainfilename == "" atveju suveiks -- rezultatas bus 1
    local fname_ix = cur_fname:find(cfgt.default.mainfilename)
    if (fname_ix)
    then
        if (fname_ix == 1)
        then
            -- atmetam tuos, kur mainfilename yra tik dalis cur_fname
            --      failo vardo (fname_ix > 1)
            proceed = true -- failo vardas sutampa tiksliai 
        else
            if (cur_fname:sub(fname_ix - 1, fname_ix - 1) == "/")
            then
                -- cur_fname yra substringas "/" .. cfgt.default.mainfilename
                proceed = true
            end
        end
    end

    --------------------- 
    -- ar pasikeitė pagrindinio failo pozicija? 
    if (proceed)
    then
        -- "top_token_tracer" callback'ui pagrindinio failo požymį
        -- tikrinti reikia tik alltokens = Calls atveju
        if (cfgt.default.alltokens ~= true) -- "no" or "calls"
        then
            -- logo įrašą išvedam, tik jei pasikeitė einamojo failo būsena --
            --      pagal tai atpažįstam perėjimą prie naujo tokeno
            --      pagrindiniam faile
            if ((cur_lnum == last_cur_lnum) and
                (cur_lpos == last_cur_lpos))
            then
                proceed = false
            end
        end
    end

    ----------------------------------------
    -- išvedinėjam
    if (proceed)
    then
        -------------------
        listed_tokcnt = listed_tokcnt + 1

        ------------ 
        -- tvarkom csname išvedimui
        local csname_tmp
        csname_tmp = ""
        if (not (csname == nil)) then csname_tmp = csname end

        -- pridedam "\\"
        if (not (csname_tmp == ""))
        then
            csname_tmp = bslash_chr .. csname_tmp
        end

        -- formuojam simbolio kodo rodymą į csname
        if
        (
            (tcmd == spacer_cmd) or         -- spacer " "
            (tcmd == letter_cmd) or         -- letter
            (tcmd == other_char_cmd) or     -- other_char
            (tcmd == char_given_cmd) or     -- char_given
            (tcmd == left_brace_cmd) or     -- left_brace "{"
            (tcmd == right_brace_cmd) or    -- right_brace "}"
            (tcmd == math_shift_cmd) or     -- math_shift "$"
            (tcmd == tab_mark_cmd) or       -- tab_mark "&"
            (tcmd == sup_mark_cmd) or       -- sup_mark "^"
            (tcmd == sub_mark_cmd) or       -- sub_mark "_"
            (tcmd == mac_param_cmd)         -- mac_param "#"
        )
        then
            if (not (csname_tmp == ""))
            then
                -- texio.write_nl(
                --      "! toktrc.lua: Character token has command name: " ..
                --      csname_tmp .. " [" .. cur_fname .. ":" .. cur_lnum .. ":"
                --      .. cur_lpos .. "]")  
                csname_tmp = csname_tmp .. " "
            end

            csname_tmp = csname_tmp .. string.char(tchcode)
        end


        local csname_out
        csname_out = cvt_out(csname_tmp)

        -- išvedinėjam trace
        if (not (toktrc_logfile == nil))
        then
            if (cfgt.default.tracetokcnt)
            then
                if (cfgt.default.tracetokcnt == "listed")
                then
                    toktrc_logfile:write(listed_tokcnt, tab_chr)
                else
                    toktrc_logfile:write(tokcnt, tab_chr)
                end
            end
            if (cfgt.default.tracefilename) then    toktrc_logfile:write(cur_fname, tab_chr) end
            if (cfgt.default.tracelpos) then        toktrc_logfile:write(cur_lnum, tab_chr, cur_lpos, tab_chr) end
            if (cfgt.default.tracetcmd) then        toktrc_logfile:write(tcmd, tab_chr) end
            if (cfgt.default.tracecmdname) then     toktrc_logfile:write(cmdname, tab_chr) end
            if (cfgt.default.tracetchcode) then     toktrc_logfile:write(tchcode, tab_chr) end
            if (cfgt.default.tracetext) then        toktrc_logfile:write(csname_out, tab_chr) end
            if (cfgt.default.tracetid) then         toktrc_logfile:write(tid, tab_chr) end
            if (cfgt.default.tracegrplev) then      toktrc_logfile:write(cur_grp_lev, tab_chr) end
            if (cfgt.default.tracemathmode) then    toktrc_logfile:write(math_mode, tab_chr) end

            if (cfgt.default.traceixlev) then       toktrc_logfile:write(format_ix_lev_str(), tab_chr) end

            if (cfgt.default.tracefont)
            then
                local font_ix = font.current()

                local font_obj = 
                    -- font.getfont(font_ix)
                    font.fonts[font_ix]

                toktrc_logfile:write(format_font_str(font_obj), tab_chr)

            end -- if (cfgt.default.tracefont)

            if (cfgt.default.tracepage) then toktrc_logfile:write(cur_page, tab_chr) end

            if (cfgt.default.traceenv)
            then
                toktrc_logfile:write(string.sub(get_exp_str(token.csname_id("@currenvir")), 3), tab_chr)
            end

            if (cfgt.default.expandmacros)
            then
                if (table.contains({call_cmd, long_call_cmd, outer_call_cmd,
                    long_outer_call_cmd}, tcmd))
                then
                        toktrc_logfile:write(get_exp_str(tid))
                end
                toktrc_logfile:write(tab_chr)
            end

            ---------------------------------
            toktrc_logfile:write(eol_chr)

        end -- if (not (toktrc_logfile == nil))

        last_cur_lnum = cur_lnum
        last_cur_lpos = cur_lpos

    end -- if (proceed)
    -------------------------------------------

    -- naikinam pasibaigusius sub/super-script lygmenis
    -- left_brace "{" – internal cur_grp_lev keičiasi po skliausto
    if (tcmd ~= left_brace_cmd)
    then
        for  ii = num_of_ix_levs, 1, -1 do
            if (cur_grp_lev <= ix_levs[ii][2])
            then
                ix_levs[ii] = nil
                num_of_ix_levs = ii - 1
            else
                break
            end
        end
    end

    ------------------- sub/super-script levels  
    if (tcmd == sup_mark_cmd) -- sup_mark "^"
    then
        num_of_ix_levs = num_of_ix_levs + 1
        ix_levs[num_of_ix_levs] = {1, cur_grp_lev}
        if (num_of_ix_levs > 1)
        then
            if (ix_levs[num_of_ix_levs - 1][1] > 0)
            then
                ix_levs[num_of_ix_levs][1] = ix_levs[num_of_ix_levs][1] +
                    ix_levs[num_of_ix_levs - 1][1]
            end
        end
    end

    if (tcmd == sub_mark_cmd) -- sub_mark "_"
    then
        num_of_ix_levs = num_of_ix_levs + 1
        ix_levs[num_of_ix_levs] = {-1, cur_grp_lev}
        if (num_of_ix_levs > 1)
        then
            if (ix_levs[num_of_ix_levs - 1][1] < 0)
            then
                ix_levs[num_of_ix_levs][1] = ix_levs[num_of_ix_levs][1] +
                    ix_levs[num_of_ix_levs - 1][1]
            end                     
        end              
    end

    ------------------
    -- math mode, names_used[] ir names_defined[] kaupimui
    if (prev_tcmd == math_shift_cmd)   -- math_shift "$"
    then
        prev_tcmd = relax_cmd
    else        
        prev_tcmd = tcmd
    end

    -------------------
    return tok
end


------------------------------------------------------------------------
-- "token_filter" callbackas
--                  perskaito eilinį srauto tokeną ir permeta valdymą
--                      į pagrindinį token_callback()
-- @return tok  grąžinamas perskaitytas įvedimo srauto tokenas
------------------------------------------------------------------------
local function token_filter_callback()

    local tok = token.get_next() -- tokenas

return token_callback(tok)
end


------------------------------------------------------------------------
-- "top_token_tracer" callbacko funkcijos paprogramė;
-- @param tok   eilinio įvedimo srauto tokeno trijų elementų table,
--                  suformuota TeX engine funkcijos get_next();
--                  ką nors keisti jau vėlu – tokenas jau nuskanuotas ir
--                  įtrauktas į TeX'o sąrašus (eqtb)
--                  permeta valdymą į pagrindinį token_callback()
------------------------------------------------------------------------
local function token_tracer_callback(tok)
    token_callback(tok)
end


------------------------------------------------------------------------
-- "stop_run" callback'as
------------------------------------------------------------------------
local function stop_run_callback()

    ------ close main log file
    if (not (toktrc_logfile == nil)) then toktrc_logfile:close() end
    toktrc_logfile = nil

    ------ dump names
    local name_tid
    local glob_csname
    local value

    ------ dump defined but unused names
    local unused_logfile = open_log_file(unused_log_fsuff)

    for name_tid, value in pairs(names_defined) do
        if (not names_used[name_tid])
        then
            unused_logfile:write(cvt_out(value[1]), tab_chr, value[2],
                                        tab_chr, value[3], eol_chr)
        end
    end

    unused_logfile:close()

    -- globaliai apibrėžti vardai mums nebeįdomūs
    -- jų įtraukimą į names_defined iškeliam už unused_logfile išdumpinimo 
    for glob_csname, value in pairs(tex.hashtokens()) do
        local tid_val = token.csname_id(glob_csname)
        local name_tid = glob_csname .. underscore_chr .. tid_val
        if (not names_defined[name_tid])
        then
            names_defined[name_tid] = {glob_csname,  tid_val, ""}
        end
    end

    for glob_csname, value in pairs(tex.primitives()) do
        local tid_val = token.csname_id(glob_csname)
        local name_tid = glob_csname .. underscore_chr .. tid_val 
        if (not names_defined[name_tid])
        then
            names_defined[name_tid] = {glob_csname, tid_val, ""}
        end
    end

    ------ dump used and defined or undefined names
    local used_logfile = open_log_file(used_log_fsuff)
    local undefined_logfile = open_log_file(undefined_log_fsuff)

    for name_tid, value in pairs(names_used) do
        local defined = true
        if (not names_defined[name_tid])
        then
            defined = false
            undefined_logfile:write(cvt_out(value[1]), tab_chr,
                                                    value[2], eol_chr)
        end
        if (defined)
        then
            used_logfile:write(cvt_out(value[1]), tab_chr, value[2], tab_chr,
                                names_defined[name_tid][3], eol_chr)
        end
    end

    used_logfile:close()
    undefined_logfile:close()
end


------------------------------------------------------------------------
-- "start_page_number" callback'as
------------------------------------------------------------------------
local function start_page_number_callback()
    cur_page = cur_page + 1
end


----------------------------------------------------
-- pradžia, inicializacija

--------------- 
-- lua tokenų trasavimo rezultatų failas
if (cfgt.default.tracelevel ~= "internal")
then
    toktrc_logfile = open_log_file(toktrc_log_fsuff)

    -- kepurė
    if (cfgt.default.tracetokcnt) then      toktrc_logfile:write("tokcnt", tab_chr) end
    if (cfgt.default.tracefilename) then    toktrc_logfile:write("filename", tab_chr) end
    if (cfgt.default.tracelpos) then        toktrc_logfile:write("lnum", tab_chr, "lpos", tab_chr) end
    if (cfgt.default.tracetcmd) then        toktrc_logfile:write("tcmd", tab_chr) end
    if (cfgt.default.tracecmdname) then     toktrc_logfile:write("cmdname", tab_chr) end
    if (cfgt.default.tracetchcode) then     toktrc_logfile:write("tchcode", tab_chr) end
    if (cfgt.default.tracetext) then        toktrc_logfile:write("text", tab_chr) end
    if (cfgt.default.tracetid) then         toktrc_logfile:write("tid", tab_chr) end
    if (cfgt.default.tracegrplev) then      toktrc_logfile:write("grplev", tab_chr) end
    if (cfgt.default.tracemathmode) then    toktrc_logfile:write("mathmode", tab_chr) end
    if (cfgt.default.traceixlev) then       toktrc_logfile:write("ixlev", tab_chr) end
    if (cfgt.default.tracefont) then        toktrc_logfile:write("font", tab_chr) end
    if (cfgt.default.tracepage) then        toktrc_logfile:write("page", tab_chr) end
    if (cfgt.default.traceenv) then         toktrc_logfile:write("environment", tab_chr) end
    if (cfgt.default.expandmacros) then     toktrc_logfile:write("expansion", tab_chr) end
    toktrc_logfile:write(eol_chr)
end

--------------------------------------
-- hookinam eventus
if (cfgt.default.tracelevel ~= "internal")
then
    if (cfgt.default.tracelevel == "filter")
    then
       callback.register("token_filter", token_filter_callback)
    else -- if (cfgt.default.tracelevel == "tracer")
        callback.register("top_token_tracer", token_tracer_callback)
    end
    callback.register("stop_run", stop_run_callback)

    callback.register("start_page_number", start_page_number_callback)
end
