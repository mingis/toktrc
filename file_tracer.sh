#! /bin/sh

# vtex 2010 4ht iosmlatex $1 draft "" "" --lua=file_tracer.lua > iosmlatex.log 2>&1
vtex 2010 luatex --fmt=lualatex --progname=latex --recorder --interaction=batchmode --lua=file_tracer.lua $1
