#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
toktrc_doc.py
    Python interfeisas su luatex toktrc.inc tokenų treiseriu.
    
    Aprašymo ištraukiklis.

Changelog:
    2013-10-09  mp  initial creation    

"""

from toktrc import TokTrc

help(TokTrc)
