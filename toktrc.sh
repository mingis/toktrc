#! /bin/sh

# export LD_LIBRARY_PATH=".:$LD_LIBRARY_PATH"
export CLUAINPUTS=/usr/lib

# cp toktrc_linux.sty toktrc_local.sty

# vtex 2012 luatex_dvi $1
# vtex 2010 luatex --fmt=lualatex --progname=lualatex --recorder --interaction=nonstopmode --lua=toktrc.lua --synctex=1 $1 > $1.log 2>&1
# vtex 2010 luatex --fmt=lualatex --progname=lualatex --recorder --interaction=nonstopmode --lua=toktrc.lua --synctex=2 --draftmode=2 $1 > $1.log 2>&1

# negalima naudoti --draftmode – neiškvies "stop_run" callbacko
# vtex 2010 luatex --fmt=lualatex --progname=latex --recorder --interaction=nonstopmode --lua=toktrc.lua $1 > $1.log 2>&1
vtex 2010 luatex --fmt=lualatex --progname=latex --recorder --interaction=batchmode --lua=toktrc.lua $1
