#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
toktrc.py
    Python interfeisas su LuaTeX toktrc.lua tokenų treiseriu.
    Ištraukia nurodytų TeX failo pozicijų būsenas kompiliavimo metu.
    Į texroot medį turi būti įdiegtas LuaTeX kompiliatorius su toktrc
    papildymais (žr. install_toktrc.bat ar install_toktrc.sh).
    Suformuoja reikalingus toktrc konfigūracijos parametrus –
    perrašo darbiniam aplanke esantį failą toktrc.cfg.
    Kompiliuoja nurodytą failą foo.tex LuaTeX kompiliatoriumi su toktrc
    papildymais.
    Iš gauto trace listingo foo.tex.toktrc.log išsitraukia ieškomas būsenas ir
    grąžina iškvietusiai programai.

Syntax:

Changelog:
    2013-10-07  mp  initial creation
    2013-10-08  mp  font decoding
    2013-10-09  mp  toktrc.lua
    2013-10-11  mp  alltokens = calls
    2013-10-29  mp  get_ms_stat() rezultato laukas 'environment'
    2013-11-08  lr  python3
    2013-11-08  lr  configparser
    2013-11-08  lr  config.ini, CWD, subprocess
    2013-11-14  mp  lowercase toktrc.cfg keys and values
    2013-11-14  mp  recompile()
    2013-11-14  mp  get_ms_stat_abs()

TODO:
    - get_ms_stat() binarinę įrašo paiešką, dabar nuosekli
        suradus artimiausią, dar važiuot atgal iki pirmo tokio paties,
        nes dabar jau tik pats pirmas atitinka pagrindinio failo tokeną,
        kiti gali būt gilesni macro iškvietimai
    - skanuoti foo.tex.used/undefined/unused.log'us
    - analizuoti foo.tex.log ir foo.log

"""

import os, sys
import subprocess
import shutil
import configparser
from collections import OrderedDict
from kppylib.common.kpcapp import KpCapp
from kppylib.common.kpstdio import KpStdIO
from kppylib.env.common.kperr import KpError, KpException, \
    KpFileNotFoundError, KpFileFormatError, KpEndOfFileError, KpSystemError


#########################################
#    Trasavimo rezultatų objekto klasė
#########################################
class TokTrc(KpCapp):

    cwd_name = 'vtexTMP'
    cfg_file_name = 'toktrc.cfg'
    log_file_suff = "toktrc.log"

    #######################
    def __init__(self, distribution, file_path, all_calls=False, CWD=None):
        """
        Constructor

        @param distribution – int, TeX failo kompiliavimo distribucija VTEX
            texroot medyje – 2010, 2012 ir pan.

        @param file_path – str, analizuojamo TeX failo vardas, pvz., "foo.tex"

        @param all_calls – bool, ar naudoti visų makrokomandų iškvietimų
            analizę – reikialingas panaudotų makrokomandų vardų failo
            formavimui (foo.tex.used.log), bet mažina veikimo greitį.

        Konstruktorius nurodytą failą sukompiliuoja ir nuskanuoja sugeneruotą
            *.toktrc.log failą į self.stat_recs[]. Informacija apie norimas
            nurodyto TeX failo pozicijas po to gaunama galimai pakartotiniais
            metodo get_ms_stat() iškvietimais.

        Pakartotinis get_ms_stat() iškvietimas TeX failo neperkompiliuoja.
        Jeigu reikia tą patį failą perkompiliuoti, kuriamas naujas TokTrc
        objektas tuo pačiu .tex failo vardu.

        self.stat_recs[] įrašai – hash lentelės (dict) su laukų pavadinimais
            iš self.stat_rec_keys[]
        """

        try:
           super(TokTrc, self).__init__()

           self.constructor_(distribution, file_path, all_calls, CWD)

        except Exception as exc:
            KpError.catch(exc)
        except:
            KpError.error(KpError.KP_E_SYSTEM_ERROR, "Unhandled exception",
                                                                       True)

    ########################
    def constructor_(self, distribution, file_path, all_calls=False, CWD=None):

        self.distr = distribution
        self.file_path = os.path.abspath(file_path)

        KpError.asserte(os.path.exists(self.file_path),
            KpFileNotFoundError("Nerastas failas {}!".format(self.file_path)))

        self.fname = os.path.split(self.file_path)[1]

        ################################
        # Darbines direktorijos kelias
        ################################

        if not CWD:
            self.CWD = KpStdIO.ensure_dir(os.path.join(
                        os.path.dirname(file_path), self.cwd_name))
        else:
            self.CWD = KpStdIO.ensure_dir(CWD)

        # reliatyvus skripto kelias
        module_path = os.path.dirname(os.path.abspath(__file__))

        shutil.copy(os.path.join(module_path, 'toktrc.lua'),
                    self.CWD)
        shutil.copy(os.path.join(module_path, 'tokinit.lua'),
                    self.CWD)
        shutil.copy(os.path.join(module_path, 'configparser.lua'),
                    self.CWD)

        self.log_fname = os.path.join(self.CWD,
                                      self.fname + "." + self.log_file_suff)

        ##############################
        #   Environmento klonas
        ##############################

        self.my_env = os.environ.copy()
        self.my_env['TEXMFCNF'] = os.path.normpath(self.conf.get('texmfcnf'))

        ##############################
        #    Komandines eilutes
        #       suformavimas
        ##############################

        # kompiliatoriaus kelias nurodytas config.ini faile
        # compiler = os.path.normpath(self.conf.get('texlive' + distribution))

        # naudojam lokalų kompiliatoriaus kelią
        if sys.platform == 'win32':
            compiler = os.path.normpath(
                os.path.join(os.path.join(module_path, "common"), sys.platform))
        else:
            compiler = os.path.normpath(os.path.join(module_path, 'common/linux'))
            # compiler = os.path.join(compiler, 'lualatex')

        self.my_env['PATH'] = compiler + self.conf.get('envpathdelim') + \
                                                        self.my_env['PATH']

        compiler = os.path.join(compiler, 'luatex')

        # kelias iki lua skripto
        lua_script = os.path.join(module_path, "toktrc.lua")

        options = ["--fmt=lualatex",
                   "--progname=latex",
                   "--recorder",
                   "--interaction=batchmode",
                   "--lua=" + lua_script]

        self.file_path = self.file_path.replace('\\','/')

        self.cmd_line = [compiler] + options + [self.file_path]

        # *.toktrc.log failo įrašo ir get_ms_stat() grąžinamos lentelės
        #       laukų pavadinimai
        self.stat_rec_keys = ['tokcnt', 'filename', 'lnum', 'lpos',
            'tcmd', 'cmdname', 'tchcode', 'text', 'tid', 'grplev',
            'mathmode', 'ixlev', 'font', 'page', 'environment']

        # Lua masyvo font.fonts[] įrašo laukų pavadinimai
        self.font_rec_keys = [
            'name', 'area', 'used', 'characters', 'checksum', 'designsize',
            'direction', 'encodingbytes', 'encodingname', 'fonts', 'psname',
            'fullname', 'header', 'hyphenchar', 'parameters', 'size',
            'skewchar', 'type', 'format', 'embedding', 'filename', 'tounicode',
            'stretch', 'shrink', 'step', 'auto_expand', 'expansion_factor',
            'attributes', 'cache', 'nomath', 'slant', 'extent']

        ##############################
        #   Pirmas kompiliavimas
        ##############################
        self.recompile(all_calls)


    #############################################
    #   Pakartotinio perkompiliavimo metodas
    #############################################
    def recompile(self, all_calls = False):
        """
        Perkompiliuoja TeX failą self.file_path

        @param all_calls – žr. konstruktorių

        Suformuoja self.stat_recs[], žr. konstruktorių
        """
        try:
            self.recompile_(all_calls)
        except Exception as exc:
            KpError.catch(exc)
        except:
            KpError.error(KpError.KP_E_SYSTEM_ERROR, "Unhandled exception",
                                                                       True)

    ########################
    def recompile_(self, all_calls = False):

        #############################
        #   Skanuojamas .tex failas – užpildomas absoliučių
        #       pozicijų masyvas self.tex_lpositions[]
        #############################
        self.scan_tex_file()

        #############################
        #   Būsimos nuskanuotos *.toktrc.log failo reikšmės,
        #       suformuos scan_log_file()
        #############################
        self.stat_recs = []

        #############################
        self.make_tok_trc_cfg(all_calls) # gaminamas toktrc.cfg

        print("... Compiling: " + " ".join(self.cmd_line))
        live = subprocess.Popen(self.cmd_line,
                                env=self.my_env,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                                cwd=self.CWD)

        turinys = live.communicate()[0]
        print(turinys)

        self.scan_log_file() # iš *.tokrc.log formuojamas stat_recs[]


    ######################################
    def scan_tex_file(self):
        """
        Skanuoja TeX išeities failą, susiskaičiuoja eilučių pozicijas į
            self.tex_lpositions[]
        """
        tex_file = open(self.file_path, 'r')

        # TODO: matyt, MAC'iniams failams bus blogai
        tex_lines = tex_file.readlines()

        self.tex_lpositions = [0]
        for line in tex_lines:
            self.tex_lpositions.append(len(line))

        for ii in range(2, len(self.tex_lpositions)):
            self.tex_lpositions[ii] = self.tex_lpositions[ii] + \
                self.tex_lpositions[ii - 1]

        tex_file.close()


    ######################################
    def make_tok_trc_cfg(self, all_calls):
        """
        Sukuria failą toktrc.cfg su pilnu trasavimo elementų sąrašu 

        @param all_calls – bool, žr. konstruktorių
        """

        config = configparser.ConfigParser(dict_type=OrderedDict)
        # tam, kad butu case sensitive
        config.optionxform = str

        config['default'] = OrderedDict((
            ('mainfilename', self.fname),
            ('tracelevel', 'tracer'),
            ('tracefilename', 'yes'),
            ('alltokens', 'calls' if all_calls else 'no'),
            ('tracetokcnt', 'all'),
            ('tracelpos', 'yes'),
            ('tracetcmd', 'yes'),
            ('tracecmdname', 'yes'),
            ('tracetchcode', 'yes'),
            ('tracetext', 'yes'),
            ('tracetid', 'yes'),
            ('tracegrplev', 'yes'),
            ('tracemathmode', 'yes'),
            ('traceixlev', 'yes'),
            ('tracefont', 'full'),
            ('tracepage', 'yes'),
            ('traceenv', 'yes')))

        self.cfg_file_path = os.path.join(self.CWD, self.cfg_file_name)

        cfg_fpath_orig = self.cfg_file_path + ".orig"
        if (os.path.exists(self.cfg_file_path)):
            if (not os.path.exists(cfg_fpath_orig)):
                shutil.copy(self.cfg_file_path, cfg_fpath_orig)

        with open(self.cfg_file_path, 'w') as configfile:
            config.write(configfile)


    ##############################
    #   Lua grazintu log failu
    #    parsinimo funkcijos
    ##############################

    def parse_font(self, font_str):
        ret_dict = {}

        font_props = font_str.split('/')
        KpError.asserte(len(font_props) ==
            len(self.font_rec_keys), KpFileFormatError())

        for nr, value in enumerate(font_props):
            font_key = self.font_rec_keys[nr]
            cur_prop = value

            if font_key in ['checksum', 'direction',
                'encodingbytes', 'tounicode']:
                    cur_prop = int(cur_prop)            # integer
            elif font_key in ['hyphenchar', 'skewchar']:
                if (cur_prop == ""): cur_prop = 0 # integer or ""
                cur_prop = int(cur_prop)
            elif font_key in ['designsize', 'size', 'slant']:
                cur_prop = float(cur_prop)      # float
            elif font_key in ['stretch', 'shrink', 'step',
                'expansion_factor', 'extent']:  # float or ""
                    if (cur_prop == ""): cur_prop = 0.0
                    cur_prop = float(cur_prop)
            # elif font_key in ['used', 'auto_expand', 'nomath']:
            #     cur_prop = cur_prop           # TODO: boolean
            # elif (font_key == 'characters'):
            #     cur_prop = cur_prop           # TODO: table
            # elif (font_key == 'fonts'):
            #     cur_prop = cur_prop           # TODO: table
            # elif (font_key == 'parameters'):
            #     cur_prop = cur_prop           # TODO: hash
            # else:
            #     cur_prop = cur_prop           # text

            ret_dict[font_key] = cur_prop

        return ret_dict


    def parse_index_level(self, ix_lev_str):
        ret_dict = []
        ix_levs = ix_lev_str.split('/')

        for nr, value in enumerate(ix_levs):
            list.append(ret_dict, int(value))

        return ret_dict


    def scan_log_file(self):
        """ Nuskanuoja *.toktrc.log failą į self.stat_recs[] """

        print("... Scanning " + self.log_fname)

        with open(self.log_fname, 'rt', encoding='utf8') as log_file:
            log_lines = log_file.read().splitlines()
            log_lines = [log_line.rstrip('\t \n').split('\t') for log_line in log_lines]

        # Patikrinama ar sutampa lauku pavadinimai
        rec_keys = log_lines.pop(0)
        KpError.asserte(self.stat_rec_keys == rec_keys,
            KpFileFormatError(str(rec_keys)))

        self.stat_recs = []

        for lnr, line in enumerate(log_lines):
            self.stat_recs.append({})
            # atsijojam paskutinį tuščią įrašą
            if ((len(line) > 1) or (line[0] != "")): 
                for nr, field in enumerate(line):
                    field_name = self.stat_rec_keys[nr]

                    if field_name in ['tokcnt', 'lnum', 'lpos', 'tcmd', 'tchcode', \
                        'tid', 'grplev', 'mathmode', 'page']:
                            value = int(field)        # integer

                    elif (field_name == 'font'):
                        value = self.parse_font(field) # list

                    elif (field_name == 'ixlev'):
                        value = self.parse_index_level(field) # list

                    else:
                        value = field               # text

                    self.stat_recs[lnr][field_name] = value

                self.stat_recs[lnr]['fpos'] = \
                    self.tex_lpositions[self.stat_recs[lnr]['lnum']] + \
                                                self.stat_recs[lnr]['lpos']


    ######################################
    def get_ms_stat(self, lnum, lpos):
        """
        Ištraukia nurodytos TeX failo self.file_path pozicijos būseną kompiliavimo
            metu.

        @param lnum, lpos – int, analizuojamo TeX failo pozicija, apie kurią
            norime sužinoti informaciją. Failo eilutės numeruojamos nuo 1,
            pozicijos eilutėje – nuo 0. Pozicijos eilutėje skaičiuojamos
            baitais, t.y., UTF-8 koduotam failui kol kas bus problemų.

        @return ret_val     grąžina hash lentelę (dict, artimiausią
            self.stat_recs[] įrašą), susidedančią iš laukų, kurių reikšmių
            prasmė atitinka *.toktrc.log failo laukus, detaliau aprašytus
            failo skaityk_toktrc.txt skyrelyje „Pranešimų failo laukų
            reikšmės“. Laukų pavadinimai yra masyve stat_rec_keys[]
            Laukų reikšmių tipai ir skirtumai nuo *.toktrc.log įrašo
            struktūros:

                'tokcnt'        – int
                'filename'      – str
                'lnum'          – int
                'lpos'          – int
                'tcmd'          – str
                'cmdname'       – str
                'tchcode'       – int
                'text'          – str
                'tid'           – int
                'grplev'        – int
                'mathmode'      – int
                'ixlev'         – list *
                'font'          – dict **
                'page'          – int
                'environment'   – str
                'diff'          – int ***
                'fpos'          – int ****

          * Matematinio indekso gyliui apibūdinti grąžinama ne teksto eilutė
                (kaip *.toktrc.log atveju), o int elementų masyvas. Jeigu
                nurodyta pozicija nėra indekso viduje, bus grąžintas vienas
                nulinis elementas (skaičius 0). Jeigu indeksai įdėtiniai, bus
                grąžinamas vienas elementas, nurodantis indekso gylį.
                Apatiniai indeksai pažymimi neigiama gylio reikšme. Daugiau
                negu vienas elementas grąžinamas sudėtinių indeksų atveju,
                pvz., A_{B^{C_{D_E}}} atveju raidės "E" tokeno lygmuo bus
                [-1, 1, -2].

         ** Einamajam fontui apibūdinti grąžinama ne teksto eilutė, o hash
                lentelė – LuaTeX fontų masyvo font.fonts įrašo atitikmuo su
                laukais "name", "size", "designsize" ir kt., žr. failo
                http://www.luatex.org/svn/trunk/manual/luatexref-t.pdf
                7-ą skyrių.

        *** Grąžinamas papildomas laukas "diff", jo reikšmė – atstumas tarp
                užsakytos analizavimo pozicijos lnum, lpos ir realiai
                faile rastos artimiausios tokeno pradžios pozicijos
                get_ms_stat()["lnum"], get_ms_stat()["lpos"]. Laukas skirtas
                atpažinti situacijas, kai nurodytos pozicijos faile nebuvo.
                Jei faile buvo rasta tiksliai sutampanti pozicija, grąžinama
                nulinė lauko reikšmė. Jei ne – grąžinamas absoliutus skirtumas
                tarp pozicijų eilutėje, o jei ir eilutės nesutapo – pridedamas
                dar ir absoliutus eilučių numerių skirtumas, padaugintas iš
                1000.

        **** Grąžinamas papildomas laukas "fpos", jo reikšmė – rastos
                pozicijos absoliutus atstumas nuo failo pradžios baitais.
                Pirmo failo baito pozicija 0, eilučių galai traktuojami kaip
                užimantys visi po 1 baitą.
        """

        try:
            # ieškom artimiausio
            nearest_dist = sys.maxsize
            nearest_ix = -1

            for nr, value in enumerate(self.stat_recs):
                dist = abs(value['lpos'] - lpos) + 1000 * \
                    abs(value['lnum'] - lnum)
                if (dist < nearest_dist):
                    nearest_dist = dist
                    nearest_ix = nr

            KpError.asserte(nearest_ix >= 0, KpEndOfFileError())

            ret_val = self.stat_recs[nearest_ix]
            ret_val['diff'] = nearest_dist

            return ret_val

        except Exception as exc:
            KpError.catch(exc)
        except:
            raise KpSystemError("Unhandled exception")
            KpError.error(KpError.KP_E_SYSTEM_ERROR, "Unhandled exception",
                                                                        True)
            sys.exit()


    ######################################
    def get_ms_stat_abs(self, fpos):
        """
        Ištraukia nurodytos TeX failo self.file_path pozicijos būseną kompiliavimo
            metu.

        @param fpos – int,  absoliuti baito pozicija nuo analizuojamo TeX
            failo pradžios, apie kurią norime sužinoti informaciją. Pirmo
            failo baito pozicija 0, eilučių galai traktuojami kaip užimantys
            visi po 1 baitą.

        @return ret_val     rezultato hash lentelė (dict, žr. get_ms_stat()).
            Grąžinamo lauko ret_val["diff"] reikšmė šiuo atveju – tiesiog
            absoliutus skirtumas tarp ieškomos nurodytos pozicijos fpos ir
            rastos artimiausios tokeno pozicijos faile.
        """

        try:
            # ieškom artimiausio
            nearest_dist = sys.maxsize
            nearest_ix = -1

            for nr, value in enumerate(self.stat_recs):
                dist = abs(value['fpos'] - fpos)
                if (dist < nearest_dist):
                    nearest_dist = dist
                    nearest_ix = nr

            KpError.asserte(nearest_ix >= 0, KpEndOfFileError())

            ret_val = self.stat_recs[nearest_ix]
            ret_val['diff'] = nearest_dist

            return ret_val

        except Exception as exc:
            KpError.catch(exc)
        except:
            KpError.error(KpError.KP_E_SYSTEM_ERROR, "Unhandled exception",
                                                                        True)
            sys.exit()


if __name__ == '__main__':
    tracer = TokTrc('2010', 'test.tex')
