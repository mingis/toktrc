:: -------------------------------------
:: toktrc python failo iškvietimas
::
:: Syntax:
::
::      toktrc_py.bat toktrc_pavyzdys.py
::

:: set PYTHONHOME=d:\bin\python26
set PYTHONHOME=d:\bin\python33
set PYTHONPATH=%~dp0;%PYTHONHOME%\Lib;%PYTHONPATH%
%PYTHONHOME%\python.exe %*
