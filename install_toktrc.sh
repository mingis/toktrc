#! /bin/bash

export instpath="."
if [ "$1" != "" ]; then export instpath="$1"; fi

# cp $(dirname $0)/toktrc_linux.sty $instpath
# cp $(dirname $0)/toktrc.inc $instpath
cp $(dirname $0)/toktrc.lua $instpath

cp $(dirname $0)/toktrc.py $instpath
cp $(dirname $0)/toktrc3.py $instpath

mkdir kppylib
cp $(dirname $0)/kppylib/__init__.py $instpath/kppylib
mkdir $instpath/kppylib/common
cp $(dirname $0)/kppylib/common/__init__.py $instpath/kppylib/common
cp $(dirname $0)/kppylib/common/kpcapp.py $instpath/kppylib/common
mkdir $instpath/kppylib/env
cp $(dirname $0)/kppylib/env/__init__.py $instpath/kppylib/env
mkdir $instpath/kppylib/env/common
cp $(dirname $0)/kppylib/env/common/__init__.py $instpath/kppylib/env/common
cp $(dirname $0)/kppylib/env/common/kperr.py $instpath/kppylib/env/common

cp $(dirname $0)/toktrc_pavyzdys.py $instpath
cp $(dirname $0)/toktrc3_pavyzdys.py $instpath

cp $(dirname $0)/toktrc.sh $instpath
chmod 777 toktrc.sh

$(dirname $0)/common/linux/install_toktrc_common.sh "$1"
