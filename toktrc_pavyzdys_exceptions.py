#!/usr/bin/env python
# coding=UTF-8

__author__ = "Mindaugas Piešina <mindaugas.piesina@vtex.lt>"
__version__ = "0.1"

# --------------------------------
"""
toktrc_pavyzdys.py
    Python interfeisas su luatex toktrc.inc tokenų treiseriu.

    Failo toktrc.py naudojimo pavyzdys.

Changelog:
    2013-10-07  mp  initial creation

"""

import sys
import traceback
from toktrc import TokTrc

# ---------------------------------
# paprastas TokTrc.GetMsStatStops() iškvietimas – nurodytos pozicijos grupės 
#   gylio sužinojimas
print TokTrc().GetMsStatStops("2010", "be_pavyzdys.tex", [[49, 36]])[0]["GrpLev"]

# ---------------------------------
# TokTrc.GetMsStatStops() iškvietimas su klaidų analize
try:
    toktrc_obj = TokTrc()

    ret_table = toktrc_obj.GetMsStatStops("2010", "be_pavyzdys.tex", [[49, 36], [50, 5]])
    assert len(ret_table) == 2, "GetMsStatStops() return value length out of range"

    for ret_row in ret_table:
        assert ret_row["Diff"] == 0
        print ret_row["Lnum"], ":", ret_row["Lpos"], "  GrpLev: ", ret_row["GrpLev"], \
             "  MathMode: ", ret_row["MathMode"], "  IxLev: ", ret_row["IxLev"]

except Exception, exc:
#    if (hasattr(exc, 'child_traceback')): print exc.child_traceback
#    print type(exc)
#    print str(type(exc))
#    print exc.args
#    print exc
    lines = format_exc().split()
    for line in lines:
        print "[", line , "]"

except:
    print "! Klaida."
