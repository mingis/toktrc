#! /bin/bash

distribution="2010"

texroot="/usr/local/texlive"
tlroot="$texroot/$distribution"

# binpath="$tlroot/bin/i386-linux"
binpath="$tlroot/bin/x86_64-linux"

#libpath=/usr/lib
libpath="$binpath"

srcpath=~/F/kp/src/vtex/tex/toktrc/src/common/linux

if [ ! -f "$binpath"/luatex_orig ]; then sudo mv "$binpath"/luatex "$binpath"/luatex_orig; fi
sudo cp "$srcpath"/luatex "$binpath"

if [ ! -f "$binpath"/dviluatex_orig ]; then sudo mv "$binpath"/dviluatex "$binpath"/dviluatex_orig; fi
sudo cp "$srcpath"/luatex "$binpath"/dviluatex

if [ ! -f "$binpath"/lualatex_orig ]; then  sudo mv "$binpath"/lualatex "$binpath"/lualatex_orig; fi
sudo cp "$srcpath"/luatex "$binpath"/lualatex

if [ ! -f "$binpath"/dvilualatex_orig ]; then  sudo mv "$binpath"/dvilualatex "$binpath"/dvilualatex_orig; fi
sudo cp "$srcpath"/luatex "$binpath"/dvilualatex

sudo cp "$srcpath"/luatex.so "$libpath"

sudo rm "$libpath"/kpathsea*.so
sudo cp "$srcpath"/kpathsea.so "$libpath"

sudo cp "$srcpath"/kpadd.so "$libpath"

# TODO: pirmąkart pagaminus formatus, išsisaugot formats aplanke pagal distribucijas
# tada formatų iš naujo negamint, o kopijuot iš formats (toms distribucijoms, kurių jau pagaminta)
vtex $distribution fmtutil --all
echo !!! Nusikopijuok formatus į formats/$distribution !!!

export LD_LIBRARY_PATH="$libpath:$LD_LIBRARY_PATH"
export LUA_CPATH="$libpath/kpadd.so;;"
