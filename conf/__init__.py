'''Konfiguraciju nuskaitymo ir nustatymo objektas'''

import os
import sys
import configparser
import codecs

class ConfigError(Exception):
    pass

class Configurations():
    '''Konfiguraciju objektas.'''

    def __init__(self):
        self.dir = os.path.abspath(os.path.dirname(__file__))
        self.static_file = os.path.join(self.dir, 'config.ini')

    def get(self, name):
        '''Istraukia kintamojo vardu name reiksme is config.ini
        failo, '''
        
        config = configparser.ConfigParser()
        config.read(self.static_file)
        
        if sys.platform == 'win32':
            static_settings = dict(config['WIN'])
        else:
            static_settings = dict(config['LINUX'])

        if name in static_settings.keys():
            return static_settings[name]
        else:
            raise ConfigError(
                'Nerastas konfiguracinis kintamasis: {}'.format(name))

